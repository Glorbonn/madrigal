library(ggplot2)
library(gridExtra)

testing_errors_work_dir <- "C:/Users/rossw/OneDrive/Documents/wendt_nn_dir/master/testingErrors"
training_errors_work_dir <- "C:/Users/rossw/OneDrive/Documents/wendt_nn_dir/master/trainingErrors"
#par(mcrow = c(2,2))

#w1[,-1]
#w1[,-1]
#w1[,1]
#length <- length(w1[,-1])
setwd(training_errors_work_dir)

temp = list.files(pattern="*.csv")

var_list = combn(temp, 1,simplify=FALSE)
#var_list

plot_list = list()
for(i in temp){
  name <- paste("p", i, sep="")

  #w2 <- c(0: (length(w1[,-1])))
  errors <- as.numeric(read.csv(file=i, sep=",", head=FALSE))
  #errors = errors[,-1]
  epochs <- c(0:(length(errors)-1))
  #w2 <- matrix(0:(length(w1[,-1])-1), nrow=1)
  #w1 <- matrix(w1[,-1], nrow=1)
  #matplot(w2[-1], w1[,-1], type="l", xlim = c(0,145), ylim = c(0,1.0), xlab="epoch", ylab="error")
  #plot(epochs,errors, type="l", xlab="epoch", ylab="error")
  
  errorepochdata <- data.frame("epochs" = epochs, "errors" = errors)
  
  p1 <- qplot(data=errorepochdata, x=epochs, y=errors, geom='line', ymin=0) + theme_bw()
  
  plot_list[[i]] = p1
  

}

n <- length(plot_list)
nCol <- floor(sqrt(n))
do.call("grid.arrange", c(plot_list, ncol=nCol))

#plot_list

#for (i in length(plot_list)-1) {
#  grid.arrange(plot_list[[i]], plot_list[[i+1]], nrow=2, ncol=2)
#}


#w2

#nrow(w2)
#nrow(w1)

#length(w2[,-1])
#length(w1[,-1])

#w1c <- c(w1[,-1])
#w1c



#matplot(w2[,-1], w1[,-1], type="l")

#matplot(c(1:5), c(1:5), type="l")


#plot(w2[-1], w1[,-1])


