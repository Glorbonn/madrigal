/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import DriverPack.Controller;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author rossw
 */
public class TestCLI {
    public static void main(String[] unused) {
        String[] testCases =                 
        { 
        /* Case 0*/      "-layerSizeRange 20 -mb 2 -yNum 1 -layerNumberRange 2 -etaRange 1e-5 -mr .7 -numberThreads 1 -in1 UCI_data\\concrete\\use\\Concrete_Data.csv -indices 5 -step 1"
                       , "-layerSizeRange 400 -yNum 3 -layerNumberRange 5 -classify -etaRange 1e-25 -numberThreads 8 -in1 UCI_data\\playing_around\\2color_true_class.csv -in2 UCI_data\\playing_around\\2color.csv"
                       , "-layerSizeRange 200 -mb 2 -yNum 10 -layerNumberRange 2 -etaRange 1e-3 -mr .99 -numberThreads 1 -in1 other_data\\mnist\\mnist_train_100.csv -indices 0 -c"
                       , "-layerSizeRange 50 -mb 2 -yNum 10 -layerNumberRange 2 -etaRange 1e-8 -mr .1 -numberThreads 1 -in1 other_data\\mnist\\mnist_train_100.csv -indices 0 -c"
                       , "-layerSizeRange 20 -mb 2 -yNum 1 -layerNumberRange 2 -etaRange 1e-2 -mr .7 -numberThreads 1 -in1 UCI_data\\concrete\\use\\Concrete_Data.csv -indices 5"
                       , "-layerSizeRange 50 -mb 2 -lnr 3 -yNum 10 -etaRange 1e-6 -mr .05 -numberThreads 1 -in1 other_data\\mnist\\mnist_train_100.csv -indices 0 -c -step 100"
                       , "-layerSizeRange 50 -mb 2 -lnr 2 -yNum 10 -etaRange 1e-5 -mr .05 -numberThreads 1 -in1 other_data\\mnist\\mnist_train_100.csv -indices 0 -c"
                       , "-layerSizeRange 200 -mb 2 -lsr 200 -lnr 2 -yNum 10 -etaRange 1e-9 -mr .2 -numberThreads 1 -in1 other_data\\mnist\\mnist_train_100.csv -indices 0 -c -step 100"
        /* Case 8*/    , "-layerSizeRange 20 -mb 2 -yNum 1 -layerNumberRange 2 -etaRange 1e-2 -mr .4 -numberThreads 1 -in1 UCI_data\\concrete\\use\\Concrete_Data.csv -indices 5 -step 100" //used to work well
                       , "-layerSizeRange 50 -mb 2 -lnr 2 -yNum 10 -etaRange 1e-2 -mr .1 -numberThreads 1 -in1 other_data\\mnist\\mnist_train_100.csv -indices 0 -c" // can get 80/ 20 with activations both sigmoid, even tho sigmoid is applied to softmax (INVALID TEST)
                       , "-layerSizeRange 50 -mb 2 -lnr 2 -yNum 10 -etaRange 1e-4 -mr .01 -numberThreads 1 -in1 other_data\\mnist\\mnist_train_100.csv -indices 0 -c" // has found 80/20 (INVALID TEST
                       , "-layerSizeRange 50 -mb 2 -lnr 2 -yNum 10 -etaRange 1e-2 -mr .8 -numberThreads 1 -in1 other_data\\mnist\\mnist_train_100.csv -indices 0 -c" 
        /* Case 12*/    , "-layerSizeRange 20 -mb 2 -yNum 1 -layerNumberRange 2 -etaRange 1e-4 -mr .1 -numberThreads 4 -in1 UCI_data\\concrete\\use\\Concrete_Data.csv -indices 5 -step 100" //has some falling error curves
        /* Case 13*/    , "-layerSizeRange 20 -mb 1 -yNum 1 -layerNumberRange 2 -etaRange 1e-4 -mr .1 -numberThreads 4 -in1 UCI_data\\concrete\\use\\Concrete_Data.csv -indices 5 -step 100" //minibatch = 1, plot saved as case 13 plot oct 24



        };
        String referenceTest10 = "-layerSizeRange 20 -mb 1 -yNum 1 -layerNumberRange 2 -etaRange 1e-5 -mr 0.4 -numberThreads 1 -in1 UCI_data\\concrete\\use\\Concrete_Data.csv -indices 5 -step 1"; // convergence behavior seen once 4.69e-01 @ epoch 2 to 2.79e-01 @ epoch 282

        
        String referenceTest9 = "-layerSizeRange 200 -mb 1 -lnr 2 -yNum 10 -etaRange 1e-5 -mr 0.05 -numberThreads 1 -in1 other_data\\mnist\\mnist_train_100.csv -indices 0 -c"; // Number correct 10,11,12 (then 11 10 9) seen once

        String referenceTest8 = "-layerSizeRange 50 -mb 1 -lnr 2 -yNum 10 -etaRange 1e-2 -mr 0.05 -numberThreads 1 -in1 other_data\\mnist\\mnist_train_100.csv -indices 0 -c"; // NaNs with sigmoid.java fixed to not modify its input parameter

        String referenceTest7 = "-layerSizeRange 200 -mb 1 -lnr 2 -yNum 10 -etaRange 1e-7 -mr 0.05 -numberThreads 1 -in1 other_data\\mnist\\mnist_train_100.csv -indices 0 -c"; // Number correct 10,11,12 (then 11 10 9) seen once

        String referenceTest5 = "-layerSizeRange 50 -mb 2 -lnr 2 -yNum 10 -etaRange 1e-2 -mr .4 -numberThreads 1 -in1 other_data\\mnist\\mnist_train_100.csv -indices 0 -c";
        String referenceTest4 = "-layerSizeRange 20 -mb 1 -yNum 1 -layerNumberRange 2 -etaRange 1e-2 -mr 0.4 -numberThreads 1 -in1 UCI_data\\concrete\\use\\Concrete_Data.csv -indices 5 -step 1";
        // DO NOT MODIFY BELOW
        String referenceTest3 = "-layerSizeRange 20 -mb 1 -yNum 1 -layerNumberRange 2 -etaRange 1e-2 -mr .05 -numberThreads 1 -in1 UCI_data\\concrete\\use\\Concrete_Data.csv -indices 5 -step 1";
        String referenceTest2 = "-layerSizeRange 20 -mb 1 -yNum 1 -layerNumberRange 2 -etaRange 1e-2 -mr .4 -numberThreads 1 -in1 UCI_data\\concrete\\use\\Concrete_Data.csv -indices 5 -step 100";
        String referenceTest = "-layerSizeRange 20 -mb 2 -yNum 1 -layerNumberRange 2 -etaRange 1e-2 -mr .4 -numberThreads 1 -in1 UCI_data\\concrete\\use\\Concrete_Data.csv -indices 5 -step 100"; //used to work well

        
        // decent run with 5.45289e-7 eta and 0.045462973 momentum with { 25, 48, 48} this is testCases[5]
        //String test = referenceTest;
        String test = referenceTest10;
        String[] args = test.split(" ");
        try {
            Controller.main(args);
        } catch (IOException | ParseException | InterruptedException ex) {
            System.out.println("ERROR IN TESTCLI");
            Logger.getLogger(TestCLI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
