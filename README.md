# Machine Learning: Matrix Neural Nets
A repository for Ross Wendt's personal neural net project. This project is for personal enrichment and is not meant to be used for serious applications. Started as a Fall 2015 class project for Dr. Sheppard's Machine Learning: Soft computing class at MSU Bozeman, MT. Credit to Angus T. and Lukas K. for the work we did during that time. (see Devlog Fall 2015 below under Past Development). Fall 2015 Development from the duration of the course can be viewed at [the github repository used at that time.](https://github.com/rosswendt/MSUMLSheppard2015/) 

# Neural Nets with Matrices
Functionality for the ANN/ backprop/ feed-forward with matrix math implemented thanks to [Brian Dolhansky's Artificial Neural Networks: Matrix Form (Part 5)](http://briandolhansky.com/blog/2014/10/30/artificial-neural-networks-matrix-form-part-5).| 

* * *

## Currently indev (8/ 17/ 2018)
*   Classification
*	Documenting current issues

* * *
## Dependencies for compiling
*	Apache Commons CLI -- version 1.3.1 : https://commons.apache.org/proper/commons-cli/

* * *

## Upcoming
*  	Genetic algorithm reimplementation

* * *

## Past Development
### Devlog Fall 2015:
* 	Initial project design and assignment from Dr. Sheppard's Undergrad Machine Learning Course
* 	Backpropagation with momentum
* 	Variable layer (& node size) Matrix Neural Net with Feed-forward
*  	Data in from CSVs

### Devlog 2016:
*       Multithreading (multiple independent concurrent experiments)
*  	~~Automated hyper-parameter search~~ No longer in use
*  	Automated parameter (hypers & weights) saving to disk with UUID and timestamp
* 	~~Error monitoring with early stopping~~ No longer in use
*  	Basic simulated annealing for some parameters
*  	~~Genetic Algorithm basic functionality~~ Refactoring in progress
*	Support for batching/ minibatching
*  	Data coupling (input data coupled to output data in Pair object)
*  	Lots of refactoring & debugging :)
*  	~~Unwrap arraylists and Doubles into primitives~~ this was unnecessary, but good practice!
*  	Basic command line arguments for small subset of program params
* 	Integrate issues with bitbucket issue tracker
*	Some hardcoded params added to command line
*	Code cleanup/ refactoring for clarity
* * *

### Devlog 2017:
* 	Refactoring and removal of old commented-out code
*	More issues added to issue tracker
* 	More parameters added to CLI
* 	Beginnings of re-implementation of genetic algorithms
*	Matrix multiplication implemented in test file using CUDA SGEMM

### Devlog 2018:
*	Initial classification development
*	SoftMax implementation
*	Classification debugging (See Issue #8)
*	Code cleanup/ refactoring for clarity

### Future plans
*  	Recurrent Neural Net
*  	Forecasting/ temporal neural net / Backpropagation through time
*  	Tensors ???
*	Simple CUDA kernels for: matrix multiply, matrix addition/ subtraction, scalar multiplication
*	Uploading all data to GPU and running as much on GPU as possible to avoid overhead from moving data between RAM and GPU memory every time a CUDA kernel is called
*	Refactoring/ parameterizing methods


#### readme.md last updated 8 / 17 / 2018

# 	Example Output


```
#!java

	**VALIDATION SET**
	Raw Error:	-2.3648686203e+04
	Number Correct: 	6743 	 Wrong: 	11899
	**TEST SET**
	Raw Error:	-2.3648686203e+04
	Number Correct: 	7494 	 Wrong: 	13219
RUNNING WITH ETA: 1.4E-9
RUNNING WITH MOMENTUM: 1.0E-9



WEIGHT MATRIX: 
477.65565793679974,695.3017763399532,170.36632736513403,715.067822698142,32.25322408761744,687.4841727001824,920.4929845435613,437.03101824702117,2.596679847170824,691.228625154244,463.0678338053631,517.4254224205368,846.7950776369969,251.41566232250867,471.25521611177413,899.7303194723872,22.284855894435186,345.0754025273124,880.9207030110962,944.6793031322134,396.965838878927,746.172347697376,229.48280626612038,157.59431282695436,781.9506139436107,490.37425264184174,452.57526117563964,980.4408456832348,946.0654667800858,207.41388313200383,815.5518099301277,940.1385494513617,51.34216497779265,394.31628268785556,251.593304890799,390.9510398544513,309.09187902042765,539.678211447226,754.1518971287841,66.31713388172045,;


797.7535400238683,377.350879811454,628.6953462959326,664.2752367069812,806.5612200797283,447.2906038064237,347.18919958431684,730.6717548964722,773.1760017968875,572.0063315095437,468.4831162426333,175.6329535603941,868.7797372217236,818.9473109080257,588.7423602621989,861.3191500183476,550.7295330455767,12.337729212389625,568.2933466607218,832.7693940008816,950.7235882413652,882.3795016722464,344.72811438951777,928.6830495371634,795.5300344231667,495.408615358271,486.8391550597523,705.2746784937227,667.682744653229,695.5040633476959,269.93267458848857,833.8771413827963,805.1489711189253,906.2127038793549,460.29529822087244,391.7569339112653,311.8254557476331,790.4676916771715,185.06482396414924,755.6451101646494,;

...


584.712368186672,554.3254005709842,728.4605285376147,96.21842212016007,757.5809436044348,71.0605558621581,517.1422611180917,489.1966462951881,383.3434526963401,817.7675258529632,454.2956448427139,732.6534233899529,907.044409655457,495.0162479328588,254.34564538594506,264.8972612860665,697.4195392123754,790.0318394258564,776.6480401420207,208.1270712104414,243.80403185619127,824.7216720810007,205.38067500312673,466.45364375651155,571.0288079863362,541.7605225784458,153.73712635552306,89.02080988755989,144.55215904535712,567.8264971187568,375.31395994135,804.9946742498498,804.6755108058953,586.2450309029281,47.810655950512036,770.9793851391396,816.5439624278064,275.7120003861219,657.6365718510975,736.4556341501567,;


951.7332939544074,427.43118380266753,358.74706272960987,29.12854019070743,627.5773758416885,939.9153377646379,305.00624531761,989.6095666550323,521.9907492077313,8.864203545269067,862.6016298996378,743.6466108336716,754.3933761698734,978.9468755762709,797.5114835801187,490.15576686290086,677.4455483987942,4.979948098409515,502.75275620145857,804.5964397032981,559.6529687393455,881.9069273250736,81.11408722626756,809.7716815338574,955.5032823871015,146.14863041464432,631.3045797050588,449.9237357359564,357.6470078589986,825.307973743658,398.92601715134055,519.0343239944374,804.7374270189036,18.877156643922422,548.4022524017475,540.5282320078151,478.8953290982505,829.5086867503703,187.97695383204592,73.11764120752662,;


...


615.1175429480847,596.2826905874799,140.60124555731502,;


681.2749668778773,718.850230253277,926.8492218778326,;


251.62239569329736,597.6864982454658,95.17689306996746,;


86.57912011434787,91.04876573285748,200.18901672153433,;


```