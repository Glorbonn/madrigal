@echo off
start java -jar nn_wendt.jar -layerSizeRange 100 -layerNumberRange 15 -etaRange 1e-15
start java -jar nn_wendt.jar -layerSizeRange 200 -layerNumberRange 10 -etaRange 1e-12
start java -jar nn_wendt.jar -layerSizeRange 100 -layerNumberRange 5 -etaRange 1e-7
start java -jar nn_wendt.jar -layerSizeRange 100 -layerNumberRange 6 -etaRange 1e-12