/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DriverPack;

import java.io.IOException;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author rossw
 */
public class ParserTester {
    public static void main(String[] args) throws IOException, InterruptedException, ParseException {
        String testIn1 = "-layerSizeRange 10 -layerNumberRange 5 -etaRange 1e-9 -momentumRange .1 -numberThreads 4 -in1 UCI_data\\concrete\\use\\Concrete_Data.csv -indices 5 -minibatch 5";
        String testIn2 = "-layerSizeRange 60 -layerNumberRange 8 -etaRange 1e-9 -momentumRange .6 -numberThreads 4 -classify -in1 UCI_data\\playing_around\\2color.csv -in2 UCI_data\\playing_around\\2color_true_class.csv -minibatch 10";
        
        String[] testInArr = testIn2.split(" ");
        
        
        Controller.main(testInArr);
    }
}
