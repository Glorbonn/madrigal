/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DriverPack;

import DataPack.DataError;
import Math.Matrix;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author rossw
 */
public class DisplayDriver {
    
    boolean trainingEnd = false;
    ArrayList trainingErrors = new ArrayList<>();
    ArrayList testErrors = new ArrayList<>();
    
    public DisplayDriver() {
        
    }

    /*
    Version 4 compatible with cuda-izing changes
     */
    public double consoleOutput(int numCorrectTest, int numWrongTest, Matrix error, DataError validation, DataError train, NeuralNetDriver neuralNetDriver, int epoch, String netCode, int outputStep) throws FileNotFoundException {
        
        double errorVal = 0;
        //numCorrectValidation = this.numCorrectValidation;
        //numCorrectTest = 0;
        double falsePositiveValidation = 0;
        double falseNegativeValidation = 0;
        double falsePositiveTest = 0;
        double falseNegativeTest = 0;
        //validationAbsError = validation.getError();//MiniLib.calcListError(errors);
        neuralNetDriver.testAbsError = neuralNetDriver.calcTestError();
        double trainAbsError = train.getError();
        int correctTrain = train.getCorrect();
        int wrongTrain = train.getWrong();
        //int correctValidation = validation.getCorrect();
        //int wrongValidation = validation.getWrong();
        int correctTest = numCorrectTest;
        int wrongTest = numWrongTest;
        for (int i = 0; i < neuralNetDriver.correctValid.size(); i++) {
            neuralNetDriver.numCorrectValidation += neuralNetDriver.correctValid.get(i);
        }
        
        if (neuralNetDriver.epoch >1) {            
            trainingErrors.add(trainAbsError);
            testErrors.add(neuralNetDriver.testAbsError);
        }
        //double validationBatchError = MiniLib.calcError(Net.errorMatrix);
        if (neuralNetDriver.epoch > 1 && (neuralNetDriver.epoch % neuralNetDriver.outputStep == 0 || trainingEnd)) {
            System.out.println("**********************");
            System.out.println("CODE: " + neuralNetDriver.Net.netCode);
            System.out.println("\nEPOCH " + neuralNetDriver.epoch + ":");
            /*
            Validation set output
             */
            /*System.out.format("%n**VALIDATION SET**%n");
            System.out.format("\tRaw Error:\t%1.10e%n",validationAbsError);
            //int numAll = validation.size() * validation.get(0).size()*validation.get(0).get(0).size() ;
            if(Controller.isClassification) {
            System.out.format("\tNumber Correct: \t%d \tWrong: \t%d%n", correctValidation, wrongValidation);
            //System.out.format("\tFalse Positive\t%1.2e \tFalse Negative\t %1.2e%n", falsePositiveValidation, falseNegativeValidation);
            }*/
            /*
            Train set output
             */
            System.out.format("%n**TRAINING SET**%n");
            System.out.format("\tRaw Error:\t%1.10e\n", trainAbsError);
            if (Controller.isClassification) {
                System.out.format("\tNumber Correct: \t%d \tWrong: \t%d%n", correctTrain, wrongTrain);
            }
            /*
            Test set output
             */
            System.out.format("%n**TEST SET**%n");
            System.out.format("\tRaw Error:\t%1.10e\n", neuralNetDriver.testAbsError);
            if (Controller.isClassification) {
                System.out.format("%n\tNumber Correct: \t%d \tWrong: \t%d%n", correctTest, wrongTest);
                //System.out.format("\tFalse Positive\t%1.2e \tFalse Negative\t %1.2e%n", falsePositiveTest, falseNegativeTest);
            }

            System.out.println("**********************");
        }
        
        return errorVal;
    }   
    
    public void trainingEndOutput(int numCorrectTest, int numWrongTest, Matrix error, DataError validation, DataError train, NeuralNetDriver neuralNetDriver, int epoch, String netCode, int outputStep) throws FileNotFoundException, IOException {
        trainingEnd = true;
        System.out.println("\nxxxxxxxxxxxxxxxxxxxxxxxxx");
        System.out.print("CODE " + netCode + " ");
        System.out.println(Thread.currentThread().getName()+" HALTED ON EPOCH " + epoch);
        consoleOutput(numCorrectTest, numWrongTest, error, validation, train, neuralNetDriver, epoch, netCode, outputStep);
        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxx");
        
        writeToDisk(trainingErrors, "trainingErrors");
        writeToDisk(testErrors, "testingErrors");
        trainingErrors.clear();
        
        trainingEnd = false;
    }
    
    public void writeToDisk(ArrayList errors, String path) throws IOException {
        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdir();
        }
        
        long currTime = System.currentTimeMillis();
        UUID uuid = UUID.randomUUID();
        File file = new File(path + "//" + "error" + "-" + uuid + ".csv");
        file.createNewFile();
        try (FileWriter writer = new FileWriter(file, true)) {
            for (int i = 0; i < errors.size(); i++) {
                writer.append(errors.get(i).toString());
                if (i < errors.size()-1) {
                    writer.append(",");          
                }
            }
            writer.append('\n');
        }
        
     
        
    }
}
