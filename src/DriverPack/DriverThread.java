package DriverPack;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ross Wendt
 */
public class DriverThread extends Thread{

    private Driver driver;
    public DriverThread (Driver in) {
        driver = in;
    }
    
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+" Start");
        try {
            driver.netDriver.runWithOutput();
            //this.nNetHelper.runWithOutput();
        } catch (IOException ex) {
            Logger.getLogger(DriverThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
