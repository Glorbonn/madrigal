package DriverPack;
import DataPack.Data;
import Math.Functions.Activation.*;
import DataPack.DataPackInterface;
import Math.Functions.Loss.AbstractLoss;
import Math.Matrix;
import NeuralNet.*;
import NeuralNet.TrainingMethod.*;
import java.util.ArrayList;

/**
 *
 * @author Ross Wendt/
 */
public class Driver {   
    private int outputSize;
    private int k;
    private float testPercentage, beta, mutationRate, crossoverRate, radius, center, xValUpperBound, xValLowerBound, exponent, upperBoundBiasWeight;
    private float upperBoundWeight;
    private float eta, momentum;
    private int outputIndex, outputStep, populationMax, populationSize, mu, lambda, epochLimit, dataSetSize, dimensionality, repetitions, numberYValuesToLearn;
    private int[] hiddenLayers;
    private AbstractFunction[] activationFunction;
    private AbstractLoss lossFunction;
    private DataPackInterface data;
    private TrainingMethodInterface trainingMethod;
    private Data dataPair;
    private boolean isClassify;
    
    /*
    Non-user defined variables
    */
    
    private Matrix inputLayer, outputLayer, targetOutput;
    private FeedforwardNetwork nNet;
    NeuralNetDriver netDriver;
    
    
    public Driver(
        ArrayList<ArrayList<Float>> in_xDataSet, 
        ArrayList<ArrayList<Float>> in_yDataSet, 
        Data in_dataPair,
        float in_testPercentage, 
        int in_outputIndex, 
        int in_outputStep, 
        int in_epochLimit, 
        
        float in_beta, 
        int in_mu, 
        int in_lambda,
        float in_radius, 
        float in_center,
        
        float in_eta, 
        float in_momentum, 
        float in_exponent,
        
        int in_dataSetSize, 
        int in_dimensionality, 
        float in_xValUpperBound, 
        float in_xValLowerBound,
        float in_upperBoundWeight, 
        float in_upperBoundBiasWeight,
        
        int[] in_hiddenLayers, 
        AbstractFunction[] in_activationFunction, 
        TrainingMethodInterface in_trainingMethod,
        AbstractLoss lossFunction,
        int in_outputSize,
        
        DataPackInterface in_data,
        
        int in_k, 
        int in_repetitions,
        boolean in_isClassify, 
        
        int in_numberYValuesToLearn,
        int in_populationSize, 
        int in_populationMax,
        
        float in_crossoverRate, 
        float in_mutationRate
            
        ) {
        testPercentage = in_testPercentage;
        outputStep = in_outputStep;
        epochLimit = in_epochLimit;
        outputIndex = in_outputIndex;
        beta = in_beta; //"amplification" factor used in DE mutation
        populationSize = in_populationSize;
        populationMax = in_populationMax;
        mutationRate = in_mutationRate;
        crossoverRate = in_crossoverRate;
        mu = in_mu;
        lambda = in_lambda;    
        radius = in_radius;
        center = in_center;
        eta = in_eta;
        momentum = in_momentum;
        exponent = in_exponent; //for use with simulated annealing
        xValLowerBound = in_xValLowerBound;
        xValUpperBound = in_xValUpperBound;
        dataSetSize = in_dataSetSize;
        dimensionality = in_dimensionality; 
        upperBoundWeight = in_upperBoundWeight;
        upperBoundBiasWeight = in_upperBoundBiasWeight;
        outputSize = in_outputSize;
        hiddenLayers = in_hiddenLayers;
        activationFunction = in_activationFunction;
        trainingMethod = in_trainingMethod;
        dataPair = in_dataPair;
        data = in_data;
        repetitions = in_repetitions;
        k = in_k;       
        isClassify = in_isClassify;
        numberYValuesToLearn = in_numberYValuesToLearn;
        
        if (isClassify) {
            outputSize = numberYValuesToLearn;
        }
        
        /*
        Program data parameters
        */
        
        /*************************************************************************/
        /* Setup below for various vars that shouldn't need to be changed        */
        /*************************************************************************/
        
        /*
        Convert first pair vals to matrices
        */
        inputLayer = new Matrix(MiniLib.getKeys(Data.getTrain().get(0).get(0)));
        outputLayer = new Matrix(new float[numberYValuesToLearn]);
        targetOutput = new Matrix(new float[numberYValuesToLearn]);
        //outputLayer = new Matrix(MiniLib.getKeys(Data.getTrain().get(0).get(0)));
        //targetOutput = new Matrix(MiniLib.getValues(Data.getTrain().get(0).get(0)));         
        nNet = new FeedforwardNetwork(inputLayer, targetOutput, hiddenLayers, upperBoundWeight, upperBoundBiasWeight, eta, exponent, epochLimit, momentum, activationFunction, trainingMethod, lossFunction);
        netDriver = new NeuralNetDriver(nNet, outputStep, testPercentage, Data.getTest(), Data.getTrain(), repetitions);         

        

    }
        
    public FeedforwardNetwork getNeuralNet() {
            return nNet;
    }

    public int getDimension() {
        return dimensionality;
    }
    
    public NeuralNetDriver getNetDriver() {
        return netDriver;
    }

    void setOutputStep(int i) {
        outputStep = i;
    }
    
    public boolean getIsClassify() {
        return isClassify;
    }

    void clear() {
        data = null;
        trainingMethod = null;
        nNet = null;
        netDriver = null;
        dataPair.getShuffledSet().clear();
        dataPair.getHashMap().clear();
        dataPair.getXData().clear();
        dataPair.getYData().clear();
    }
    
    public float getCrossoverRate() {
        return crossoverRate;
    }
    
    public int getPopulationSize() {
        return populationSize;
    }
    
    public float getMutationRate() {
        return mutationRate;
    }
    
    public int getPopulationMax() {
        return populationMax;
    }

}