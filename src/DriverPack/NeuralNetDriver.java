package DriverPack;
import DataPack.DataError;
import Math.Functions.Activation.AbstractFunction;
import Math.Functions.Loss.AbstractLoss;
import Math.Matrix;
import NetworkTracker.MatrixNeuralNetTracker;
import NeuralNet.FeedforwardNetwork;
import NeuralNet.TrainingMethod.TrainingMethodInterface;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Double.NaN;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import static java.util.Collections.list;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javafx.util.Pair;
public class NeuralNetDriver {     
    private DisplayDriver display = new DisplayDriver();
    private ArrayList<Float> errorAnalysis = new ArrayList<>();
    private ArrayList<Float> errorAnalysisDelta = new ArrayList<>();
    float avg = -100000000;
    float testAbsError = 0;
    float validationAbsError = 0;
    private int stopCounter = 0; //used to stop early if reaches 5 (hardcoded)
    int numWrongValidation = 0;
    int numCorrectValidation;
    int numCorrectTest = 0;
    int numWrongTest = 0;
    int index = 0;
    int counter = 0;
    int count = 0;
    
    DataError testError, validationError;
    //int nullCounter = 0;
    //private Matrix errorMatrix;
    ArrayList<Integer> correctValid = new ArrayList<>();
    float lastSum = 0;
    public int epoch = 1;
    private int repetitions = 5;
    public int outputStep;
    boolean runWithOutput = true;
    public FeedforwardNetwork Net;
    float checkError;
    ArrayList<Float> errors;
    private ArrayList<ArrayList<Pair<Matrix, Matrix>>>  validation;
    private ArrayList<ArrayList<Pair<Matrix, Matrix>>> test;
    private ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>> train;
    private boolean geneticFlag;

    long Time;
    float timeElapsed;
    
    public NeuralNetDriver(FeedforwardNetwork inNeuralNet, int inOutputStep, double testPercentage, 
            ArrayList<ArrayList<Pair<Matrix, Matrix>>> test,
            ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>> train,
            int repetitions) {
        Net = inNeuralNet;
        outputStep = inOutputStep;
        this.train = train;
        this.validation = validation;
        this.test = test;
        this.geneticFlag = geneticFlag;
        this.repetitions = repetitions;
        //train = Data.train;
        //validation = Data.validation;
        //shuffled = Driver.dataPair.getShuffledSet();
        checkError = 0;
        errors = new ArrayList<>();
        /*
        Populate train, test, and validation in the partition function
        */
        //partitionTest(this);
        //partition(this);
    }   
    

    public void runTest(boolean doOutput) throws FileNotFoundException, IOException {
        if ( doOutput == true ) {
            runWithOutput();
        } else {
            //NOT IMPLEMENTED runTestWithoutOutput();
        }
    }    
    public void runWithOutput() throws FileNotFoundException, IOException {
        //for (int epoch = 0; epoch < Net.getEpochLimit(); epoch++) {
            //System.out.println("Epoch" + epoch + ":");
        //while (errorAnalysis()) {

        runTrainingMethod();

        //}
        //}
    }    

    private boolean errorAnalysis() {
        if (errors.contains(NaN) || errorAnalysis.contains(NaN)) {
            return false;
        }
        if (errorAnalysis.size() > 21) {
            avg = errorAnalysis.get(errorAnalysis.size()-20) - errorAnalysis.get(errorAnalysis.size() - 1); 
            errorAnalysisDelta.add(Math.abs(avg));
            //System.out.format("%nVALIDATION ERROR SLOPE: %1.2e%n", avg);
            if (avg < 0 ) {

            }



        }
        
        if (errorAnalysisDelta.size() > 21) {
            boolean errorDecreasing = true;
            if (errorAnalysisDelta.get(errorAnalysisDelta.size() - 20) > errorAnalysisDelta.get(errorAnalysisDelta.size() -1)) {
                errorDecreasing = false;
            }
            if (errorDecreasing == false) {
                //Net.momentumParameter = Net.momentumParameter*1.05;
                //Net.eta = Net.eta * 1.05;
                //System.out.println("POSSIBLE LOCAL MINIMA: ADDING ONE TO STOPCOUNTER: " + Net.momentumParameter);
                stopCounter++;
            }
            
            /*
            Check for small delta to see if net hasn't changed
            */
            
            if (Math.abs(errorAnalysisDelta.get(errorAnalysisDelta.size() - 20) - errorAnalysisDelta.get(errorAnalysisDelta.size() -1)) < 1e-9) {
                return false;
            }
           
        }
        
        for (int i = 0; i < errorAnalysisDelta.size(); i++) {

            //System.out.print(errorAnalysisDelta.get(i) + " ");
        }
        //System.out.println(" \n");
        
        if (stopCounter > Controller.autoStopTolerance) {
            //System.out.println("ERROR TOLERANCE EXCEEDED, EXITING CURRENT NET...");
            return false;
        }
        
        if (errorAnalysis.size() < 5) {
            return true;
        } else {
            if (avg < -1.0 * Controller.epochErrorDelta) {
                return true;
            }
        }

        return true;
    }
    
    public float calcTestError() throws FileNotFoundException {
        ArrayList<Float> returnArray = new ArrayList<>();
        float error = 0;
        numCorrectTest = 0;
        numWrongTest = 0;

        checkNanAndReturnToAutodriver(Net);

        for (ArrayList<Pair<Matrix,Matrix>> b : test) { 
            Net.output.setVals(Net.dataPropagation(b));
            for (int m = 0; m < b.size(); m++) {
                float[] targetOutputVal = MiniLib.getValues(b).get(m).getArray()[0];
                float[] output = Net.output.getArray()[m];//AutoDriver.getValues(b).get(m).getArray()[0];

                if (MiniLib.isCorrect(targetOutputVal, output)) {
                    numCorrectTest++;
                } else {
                    numWrongTest++;
                }
            }
            returnArray.add(MiniLib.calcError(Net.errorMatrix));
        }
        
        for (int i = 0; i < returnArray.size(); i++) {
            error += returnArray.get(i);
        }
        error = error / returnArray.size();
        error = (float) Math.pow(error, 0.5f);
        return error;
    }
    
    public void  runTrainingMethod() throws FileNotFoundException, IOException {
        TrainingMethodInterface T = Net.getTrainingMethodInterface();
        Matrix errorMatrix = new Matrix(); //constructed inside loop below
        int batchCounter = 0; //used only for console output
        int holdOutIndex = 0;
        
        DataError trainDataError = new DataError(); //this will get overwritten with correct vals
        /*
        Stop if overfitting
        */
        
        do {

            /*
            For each subset, run a training/ validation in accordance with KFold
            */
            counter = 0;
            //System.out.println("k for k folds is " + Controller.in_k);
            for (int repetition = 0; repetition < repetitions; repetition++) {
                holdOutIndex = 0;
                // need to shuffle every time
                // flatten train set to prep for shuffling
                int outerSize = train.size();
                int innerSize = train.get(0).size();
                int innerMostSize = train.get(0).get(0).size();
                ArrayList flattener = new ArrayList<>();
                train.forEach(flattener::addAll);
                
                ArrayList innerFlattener = new ArrayList<>();
                for ( Object someList : flattener ) {
                    innerFlattener.addAll((Collection) someList);
                }
                
                // shuffle set
                Collections.shuffle(innerFlattener);
                
                // replace train with shuffled set
                
                ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>> shuffled = new ArrayList<>();
                for (int i = 0; i < outerSize; i++) {
                    ArrayList inner = new ArrayList<>();
                    for (int j = 0; j < innerSize; j++) {
                        ArrayList innerMost = new ArrayList<>();
                        for (int k = 0; k < innerMostSize; k++) {
                            innerMost.add(innerFlattener.get(i*j*k + k));
                        }
                        inner.add(innerMost);
                    }
                    shuffled.add(inner);
                }
                //train = shuffled;
                
                
                // re-chunk train set into proper sizes
                for (int k = 0; k < Controller.in_k ; k++) {
                    ArrayList<ArrayList<Pair<Matrix, Matrix>>> kthFold = shuffled.get(k); 
                    //System.out.println("kth fold is size: " + kthFold.size());
                    /*
                    Backprop code: calls feedforward & applymethod for each datum/ batch
                    */
                    
                    //System.out.println("HOLDOUT INDEX IS: " + holdOutIndex);
                    //System.out.println("K in outer loop is: " + k);

                    for (int j = 0; j < kthFold.size(); j++) {
                        //System.out.println("Size in inner loop is: " + kthFold.get(0).size());
                        ArrayList<Pair<Matrix,Matrix>> batch = kthFold.get(j);
                        if (k == holdOutIndex) { //Run validation on the held out kth subset
                            //skip holdOut
                            validationError = findError(train.get(k));   
                            break;
                        } else if (k != holdOutIndex) //Do training on non-held out kth subset
                            try {
                                count ++;
                                try {
                                    checkNanAndReturnToAutodriver(Net);
                                } catch (FileNotFoundException ex) {
                                    Logger.getLogger(NeuralNetDriver.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                Net.dataPropagation(batch);
                                /*
                                Backpropagation step
                                */
                                //System.out.println("DOING BACKPROP");
                                errorMatrix = T.applyMethod(batch, Net, this);
                                errors.add(MiniLib.calcError(errorMatrix));
                                checkNanAndReturnToAutodriver(Net);
                            } catch (FileNotFoundException ex) {
                                Logger.getLogger(NeuralNetDriver.class.getName()).log(Level.SEVERE, null, ex);
                            }
                    }
                }
                batchCounter = 0;
                holdOutIndex++;
                addError(errors);
                try {
                    display.consoleOutput(numCorrectTest, numWrongTest, errorMatrix, validationError, trainDataError, this, epoch, Net.netCode, outputStep);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(NeuralNetDriver.class.getName()).log(Level.SEVERE, null, ex);
                }
                epoch++;

                

                try {
                    weightsToDisk(errorMatrix);
                } catch (IOException ex) {
                    Logger.getLogger(NeuralNetDriver.class.getName()).log(Level.SEVERE, null, ex);
                }
                trainDataError = findTrainError(train);
                testError = findError(test);
            }
        } while (epoch <= 1000);//((epoch >= 1 && testError.getError() >= trainDataError.getError()) && errorAnalysis() );//&& epoch <= 50);
        //Training has ended
        /*try {
            errorsToDisk(errors); 
        } catch (IOException ex) {    
            Logger.getLogger(NeuralNetDriver.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        errors.clear();
        DataError testDataError = new DataError(); //save test error data 
        //MatrixNeuralNetTracker tracker = new MatrixNeuralNetTracker(epoch, trainDataError, testDataError, Net.weights.clone()); //save all error data in new MatrixNeuralNetTracker object  
        display.trainingEndOutput(numCorrectTest, numWrongTest, errorMatrix, validationError, trainDataError, this, epoch, Net.netCode, outputStep); //display traning end information in console
        //Controller.addTracker(tracker); //add tracker to history of trackers
        epoch = 1; //reset epoch to 1 for next training process 
    }
        
    public DataError findError(ArrayList<ArrayList<Pair<Matrix, Matrix>>> in) {
        ArrayList<Float> errorHelper = new ArrayList<>();
        AbstractLoss lossFunction = this.Net.lossFunction;
        float error = 0.0f;
        int correct = 0;
        int wrong = 0;
        for (ArrayList<Pair<Matrix,Matrix>> b : in) { 
            Net.output.setVals(Net.dataPropagation(b));
            for (int m = 0; m < b.size() ; m++) {
                float[] targetOutputVal = MiniLib.getValues(b).get(m).getArray()[0];
                float[] output = Net.output.getArray()[m];//AutoDriver.getValues(b).get(m).getArray()[0];
                //Matrix computedLoss = lossFunction.computeLoss(new Matrix(targetOutputVal), new Matrix(output));

                //if (MiniLib.isCorrect(targetOutputVal, output)) {
                if (MiniLib.isCorrect(targetOutputVal, output)) {
                    correct++;
                } else {
                    wrong++;
                }
            }
            errorHelper.add(MiniLib.calcError(Net.errorMatrix));
        } 
        for (int i = 0; i < errorHelper.size(); i++) {
            error += errorHelper.get(i);
            }
        error = error / errorHelper.size();
        error = (float) Math.pow(error, 0.5);
        
        DataError e = new DataError(correct, wrong, error);
        
        return e;
    }
    
    private DataError findTrainError(ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>> in ) {
        ArrayList<Float> errorHelper = new ArrayList<>();
        ArrayList<Float> squaredErrors = new ArrayList<>();
        int numCorrect = 0;
        int numWrong = 0;
        float error = 0.0f;
        double squaredError = 0.0f;
        
        for (ArrayList<ArrayList<Pair<Matrix,Matrix>>> kthSubset : in ) {
            for (ArrayList<Pair<Matrix,Matrix>> batch : kthSubset ) {
                Net.output.setVals(Net.dataPropagation(batch));
                for (int element = 0; element < batch.size(); element ++) {
                    float[] targetOutputVal = MiniLib.getValues(batch).get(element).getArray()[0];
                    float[] output = Net.output.getArray()[element];
                    //float[] error = output - targetOutputVal;
                    for (int i = 0; i < targetOutputVal.length; i++) {
                        squaredError = Math.pow((double) output[i] - (double) targetOutputVal[i], 2.0);
                        squaredErrors.add((float) squaredError);
                    }
                    
                    if (MiniLib.isCorrect(targetOutputVal, output)) {
                        numCorrect++;
                    } else {
                        numWrong++;
                    }
                }
            }
            //errorHelper.add(MiniLib.calcError(Net.errorMatrix));
        }
        
        for (int i = 0; i < squaredErrors.size(); i++) {
            error += squaredErrors.get(i);
        }
        error = error / squaredErrors.size();
        error = (float) Math.pow(error, 0.5f);
        
        /*for (int i = 0; i < errorHelper.size(); i++) {
            error += errorHelper.get(i);
            }
        error = error / errorHelper.size();
        error =  (float) Math.pow(error, 0.5f);*/

        DataError out = new DataError(numCorrect, numWrong, error);
        return out;
    }
    
    public void errorsToDisk(ArrayList<Float> errors) throws FileNotFoundException, IOException {
        long currTime = System.currentTimeMillis();
        File file = new File("savedError//error.csv");
        file.createNewFile();
        FileWriter writer = new FileWriter(file, true);
        writer.append('\n');
        for (int i = 0; i < errors.size(); i++) {
            writer.append(errors.get(i).toString());
            if (i < errors.size()-1) {
                writer.append(",");          
            }
        }
        
        writer.close();
                    
                    
                  
        
        
    }
    
    public void weightsToDisk(Matrix errorMatrix) throws FileNotFoundException, IOException {
          
        
        //PrintStream netOut = new PrintStream(new FileOutputStream(new File(new String("savedWeights//net" + System.currentTimeMillis()) + ".txt")));
        if (!errorAnalysis.contains(NaN) && Controller.saveWeights == true) {
            //Clock D = Clock.systemUTC();
            //ZonedDateTime zdt =  ZonedDateTime.now();
            //String currTime = D.instant().toString();
            long currTime = System.currentTimeMillis();
            File file = new File("savedWeights//net" + Net.netCode + "-" + currTime + ".zip");

            //BufferedInputStream origin = new BufferedInputStream(new InputStream());
            //FileOutputStream F = new FileOutputStream(file);
            FileOutputStream fileStream = new FileOutputStream(file);
            ZipOutputStream zip = new ZipOutputStream(fileStream);
            ZipEntry ZE = new ZipEntry("net" + Net.netCode + currTime + ".netout");
            zip.putNextEntry(ZE);
            //ZipEntry ze = new ZipEntry("savedWeights//net" + System.currentTimeMillis() + File.separator + ".zip");
            //System.setOut(netOut);
                //Z.
            zip.write(("RUNNING WITH ETA: " + Net.getEta() + "\n").getBytes());
            zip.write(("RUNNING WITH MOMENTUM: " + Net.momentumParameter + "\n").getBytes());
            zip.write(("\nRUNNING WITH LAYER TOPOLOGY: ").getBytes());
            zip.write(("{ ").getBytes());
            for (int i : Net.hiddenLayers ) {
                zip.write((" " + i + " ").getBytes());

            }
            /*zip.write((" } ").getBytes());
            zip.write(("\nVALIDATION ERROR: " + validationAbsError).getBytes());
            zip.write(("\nTEST ERROR: " + testAbsError + "\n").getBytes());
            if (Controller.isClassification) {
                zip.write(("\nVALIDATION VALID : " + numCorrectValidation).getBytes());
                zip.write(("\nTEST VALID : " + numCorrectTest + "\n").getBytes());
            }*/
            zip.write(("\n\n\nWEIGHT MATRIX: ").getBytes());
            for (Matrix M : Net.weights) {
                for (float weightArrays[] : M.getArray()) {
                    for (double weights : weightArrays) {
                        zip.write((weights + ",").getBytes());
                    }
                    zip.write((";\n\n\n").getBytes());
                }
            }

            //F.flush();
            //Z.close();
        }

        //Z.closeEntry();
        
            //outputErr4(errorMatrix);
        /*
        Repeat some info to console:
        */
        //System.setOut(Controller.console);
        //outputErr3();
        
        /*System.out.format("RUNNING WITH ETA: " + Net.eta + "%n");
        System.out.format("RUNNING WITH MOMENTUM: " + Net.momentumParameter + "%n");
        System.out.format("%nRUNNING WITH LAYER TOPOLOGY: ");
        System.out.print("{ ");
        for (int i : Net.hiddenLayers ) {
            System.out.print(" " + i + " ");          
        }
        System.out.print(" } ");*/
    }
    
    private void addError(ArrayList<Float> errorList) {
        float error = 0;
        for (int i = 0; i < errorList.size(); i++) {
            error += errorList.get(i);
        }
        error /= errorList.size();
        errorAnalysis.add(error); 
    }
    
    
    
    public ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>>  getTrain() {
        return train;
    }
    
    public ArrayList<ArrayList<Pair<Matrix, Matrix>>>  getValidation() {
        return validation;
    }
 
    /*
    This is basically an awful goto and needs to be excised 
    */
    private void checkNanAndReturnToAutodriver(FeedforwardNetwork Net) throws FileNotFoundException {
        //if (Double.isNaN(Net.W[0].getArray()[0][0])) 
        //    System.out.println("NaN VAL ERR");
        //    Controller.RunNew();
        //if (Net.output.getArray()[0][0] != Net.output.getArray()[0][0] || (errors.get(2) != errors.get(2))) {
         //   Controller.RunNew();
        //}
    }
}

