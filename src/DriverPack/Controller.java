package DriverPack;

import DataPack.Data;
import DataPack.DataPackInterface;
import DataPack.Read;
import Math.Functions.Activation.*;
import Math.Functions.Loss.*;
import NetworkTracker.MatrixNeuralNetTracker;
import NeuralNet.TrainingMethod.Backpropagation;
import NeuralNet.TrainingMethod.Evolutionary.DifferentialEvolution;
import NeuralNet.TrainingMethod.Evolutionary.EvolutionStrategy;
import NeuralNet.TrainingMethod.Evolutionary.GeneticAlgorithm;
import NeuralNet.TrainingMethod.TrainingMethodInterface;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.*;

/**
 *
 * @author Ross Wendt
 */
public class Controller {
    //public static String[] input = new String[10];
    
    public static int windowSize = 7;
    public static boolean windowFlag = false;
    public static ArrayList<MatrixNeuralNetTracker> tracker = new ArrayList<>();
    public static int in_outputSize = 1; // should not be a manual param
    public static float validationSize = 0.10f;
    public static int populationSize, populationMax; //not used
    public static int minibatch = 200; //minibatch size
    static ArrayList<ArrayList<Float>> in_xDataSet; //X data
    static ArrayList<ArrayList<Float>> in_yDataSet; //Y data
    static Data rawData; //the rawData read from file into program as primitives
    static Data in_dataPair; //data pairs (X,Y) created by Data.java
    private static int trialCounter; 
    public static PrintStream console; //system.out.print target for standard console output
    public static ArrayList<Driver> DriverPopulation = new ArrayList<>(); //not used//population of nets to consider for genetic interface hyperparam optimizer
    public static Iterator<Driver> DriverIterator; //iterator for going through drivers (for diff nets)
    private static int numTrials = 16; //number of trials to create automatically
    public static double in_testPercentage = .2; //var for amount of data to use for testing
    public static int in_k = 2; //number of k folds
    public static double epochErrorDelta = 1e-6; //not used
    public static boolean isClassification = false; //flag for classification
    public static int yNum = 10;
    public static int autoStopTolerance = 3; //once reached, stop net and go to next
    public static boolean saveWeights = false; //flag for saving weights to disk
    public static ArrayList<Integer> learnIndices = new ArrayList<>();
    private static BlockingQueue B;
    private static ExecutorService E;
    private static int numThreads;
    private static int msTimeout; //thread timeout
    private static String inputFile1;
    private static String inputFile2;
    private static TrainingMethodInterface in_trainingMethod = new Backpropagation(); //default trainingMethod
    private static AbstractLoss lossFunction = new SquaredError(); //default loss function
    private static int outputStep = 1;

    public static void main(String[] args) throws IOException, ParseException, InterruptedException {

        /*
         vals are randomly chosen from [hardcoded_lower_bound, etaRange]
        */
        int rangeNumLayers = 2;
        int rangeLayerSize = 5;
        float momentRange = 1e-4f;
        float etaRange = 1e-5f; 
        
                
        /*
        File loading information for debugging when CLI parser is not in use
        */
        String trainingA = "UCI_data\\playing_around\\trainingA.csv";
        String trainingB = "..\\UCI_data\\playing_around\\trainingB.csv";
        String twoColor = "UCI_data\\playing_around\\2color.csv";
        String twoColor_class = "UCI_data\\playing_around\\2color_true_class.csv";

        String airfoil = "UCI_data\\airfoil\\use\\airfoil_self_noise.dat";
        String concrete = "UCI_data\\concrete\\use\\Concrete_Data.csv";
        String energy = "UCI_data\\energy\\use\\ENB2012_data.csv";
        String gas = "UCI_data\\gas\\use\\000_Et_H_CO_n";
        String redwine = "UCI_data\\wine\\use\\winequality-red.csv";
        String slump = "UCI_data\\slump\\use\\slump_test.data";
        String yacht = "UCI_data\\yacht\\use\\yacht_hydrodynamics.data";
        String fires = "UCI_data\\fires\\use\\forestfires.csv";
        String music = "UCI_data\\music\\use\\tracks.txt";
        String cbm = "UCI_data\\cbm\\use\\cbm.txt";
        
        /*
        exclude certain elements from the input file to be used as target output.
        */

        List<Integer> exclude_concrete = asList(5);
        List<Integer> exclude_airfoil = asList(5);
        List<Integer> exclude_energy = asList(5);
        List<Integer> exclude_gas = asList(1);
        List<Integer> exclude_redwine = asList(11);
        List<Integer> exclude_slump = asList(9);
        List<Integer> exclude_yacht = asList(6);
        List<Integer> exclude_fires = asList(12);
        List<Integer> exclude_music = asList(25);
        List<Integer> exclude_cbm = asList(14);

        
        /*
        apache commons cli option setup for command line arguments
        */
        CommandLineParser parser = new DefaultParser();
        Option etaRangeArg = new Option("er", "etaRange", true, "set range for etalist");
        Option momentumRangeArg = new Option("mr", "momentumRange", true, "set range for momentlist");
        Option layerSizeRangeArg = new Option("lsr", "layerSizeRange", true, "set range for layer size");
        Option layerNumberRangeArg = new Option("lnr", "layerNumberRange", true, "set range for number of layers");
        Option saveWeightsArg = new Option("save", "saveWeights", false, "flag to save weights");
        Option classifyArg = new Option("c", "classify", false, "flag to do classification");
        Option file1InputArg = new Option("in1", "inputFile", true, "set to file to read in (this is used as X data if a second input is used)");
        Option learnIndicesArg = new Option("indices", "learnInputIndices", true, "set which indices (columns) of fileInput to 'learn'");
        Option numThreadsArg = new Option("threads", "numberThreads", true, "set the number of threads to run in parallel");
        Option file2InputArg = new Option("in2", "readDataPath2", true, "set the Path to the Y output data (to be learned)");
        Option miniBatchSizeArg = new Option("mb", "minibatch", true, "set the size of the minibatches");
        Option windowSizeArg = new Option("w", "windowSize", true, "set range size for sliding window");
        Option trainingMethodArg = new Option("t", "trainingMethod", true, "select training method from available options: backprop, genetic, evolutionstrat, diffevolution");
        Option geneticArg = new Option("gen", "geneticMethod", false, "flag to run genetic training method instead of backpropagation");
        Option yNumArg = new Option("yNum", "numberY", true, "set the number of features to learn");
        Option lossFunctionArg = new Option("loss", "lossFunction", true, "set the loss function for use in training.");
        Option outputStepArg = new Option("step", "outputStep", true, "set which epochs to do console output (every arg steps)");
        Option hiddenLayerActivationFunction = new Option("middleAct", "middleActivations", true, "choose the activation functions for all hidden layers except the last.");
        Option lastLayerActivationFunction = new Option("lastAct", "lastActivation", true, "choose the activation function for the last layer.");
        
        
        Options optns = new Options();
           
        optns.addOption(classifyArg);
        optns.addOption(etaRangeArg);
        optns.addOption(momentumRangeArg);
        optns.addOption(layerSizeRangeArg);
        optns.addOption(layerNumberRangeArg);  
        optns.addOption(saveWeightsArg);
        optns.addOption(file1InputArg);
        optns.addOption(file2InputArg);
        optns.addOption(learnIndicesArg);
        optns.addOption(numThreadsArg);
        optns.addOption(miniBatchSizeArg);
        optns.addOption(windowSizeArg);
        optns.addOption(trainingMethodArg);
        optns.addOption(geneticArg);
        optns.addOption(yNumArg);
        optns.addOption(lossFunctionArg);
        optns.addOption(outputStepArg);

        
        /*
        apache commons cli parser and interrogator
        */
        
        CommandLine cmd = parser.parse(optns, args);
        
        if (cmd.hasOption(etaRangeArg.getOpt())) 
            etaRange = Float.parseFloat(cmd.getOptionValue(etaRangeArg.getOpt()));
        if (cmd.hasOption(momentumRangeArg.getOpt())) 
            momentRange = Float.parseFloat(cmd.getOptionValue(momentumRangeArg.getOpt()));
        if (cmd.hasOption(layerSizeRangeArg.getOpt())) 
            rangeLayerSize = Integer.parseInt(cmd.getOptionValue(layerSizeRangeArg.getOpt()));
        if (cmd.hasOption(layerNumberRangeArg.getOpt()))
            rangeNumLayers = Integer.parseInt(cmd.getOptionValue(layerNumberRangeArg.getOpt()));
        if (cmd.hasOption(saveWeightsArg.getOpt()))
            saveWeights = true;
        if (cmd.hasOption(file1InputArg.getOpt()) && !cmd.hasOption(file2InputArg.getOpt())) {
            //in1 = Arrays.toString(cmd.getOptionValues(file1InputArg.getOpt()));
            inputFile1 = cmd.getOptionValue(file1InputArg.getOpt());
            if (cmd.hasOption(learnIndicesArg.getOpt())) {
                for (String S : cmd.getOptionValues(learnIndicesArg.getOpt())) {
                    //String[] debug = cmd.getOptionValues(learnIndicesArg.getOpt());
                    learnIndices.add(Integer.parseInt(S));
                }
            } else {
                System.out.println("Index required for single input file");
                Thread.sleep(4000);
                System.exit(0);
            }
        } else if (cmd.hasOption(file1InputArg.getOpt()) && !cmd.hasOption(file2InputArg.getOpt())) {
            inputFile1 = cmd.getOptionValue(file1InputArg.getOpt());
            inputFile2 = cmd.getOptionValue(file2InputArg.getOpt());
        } else {
            //default vals
            inputFile1 = twoColor_class;
            inputFile2 = twoColor;
            //learnIndices.add(exclude_airfoil.get(0));
        }
        if (cmd.hasOption(numThreadsArg.getOpt())) {
            numThreads = Integer.parseInt(cmd.getOptionValue(numThreadsArg.getOpt()));
        } else {
            numThreads = 2;
        }
        if (cmd.hasOption(miniBatchSizeArg.getOpt())) {
            minibatch = Integer.parseInt(cmd.getOptionValue(miniBatchSizeArg.getOpt()));
        } else {
            minibatch = 10;
        }
        if (cmd.hasOption(classifyArg.getOpt())) {
            isClassification = true;
            lossFunction = new CrossEntropy();
        } else {
            isClassification = false;
            lossFunction = new SquaredError();
        }
        if(cmd.hasOption(windowSizeArg.getOpt())) {
            windowSize = Integer.parseInt(cmd.getOptionValue(windowSizeArg.getOpt()));
            windowFlag = true;
        }
        if (cmd.hasOption(trainingMethodArg.getOpt())) {
            switch(trainingMethodArg.getValue()) {
                case "backprop": case "backpropagataion": case "BackPropagation": 
                    in_trainingMethod = new Backpropagation();
                    break;
                case "genetic": case "geneticAlgorithm":
                    in_trainingMethod = new GeneticAlgorithm();
                    break;
                case "diffevolution": case "differentialEvolution":
                    in_trainingMethod = new DifferentialEvolution();
                    break;
                case "evostrategy": case "evolutionStrategy":
                    in_trainingMethod = new EvolutionStrategy();
                    break;
                default:
                    in_trainingMethod = new Backpropagation();
                    break;
            }
            
        }
        if (cmd.hasOption(yNumArg.getOpt())) {
            yNum = Integer.parseInt(cmd.getOptionValue(yNumArg.getOpt()));
        }
        if (cmd.hasOption(lossFunctionArg.getOpt())) {
            switch(lossFunctionArg.getValue()) {
                case "mse": case "MeanSquaredError": case "meansquarederror":
                    lossFunction = new SquaredError();
                case "crossentropy": case "entropy": case "ce":
                    lossFunction = new CrossEntropy();
            }
        }
        if (cmd.hasOption(outputStepArg.getOpt())) {
            outputStep = Integer.parseInt(cmd.getOptionValue(outputStepArg.getOpt()));
        } else {
            outputStep = 1;
        }
        
        
        /*
        Thread options
        */
        //numThreads = 1;
        //B = new LinkedBlockingQueue(numThreads);
        msTimeout = 500;
        System.out.println("NUMBER OF THREADS: " + numThreads);
        E = Executors.newFixedThreadPool(numThreads);
        
        
        /*
        default options if no arg passed
        */
        //int rangeNumLayers = 2;
        //int rangeLayerSize = 80;
        //double momentRange = 1e-1;
        //double etaRange = 1e-9;
        


        trialCounter = 0;
        console = new PrintStream(System.out);
        /*
        Manual settings for classification
        */
        //int in_numClasses = 3;
        
        /*
        General Parameters
        */        
        float testPercentage = 0.10f;
        int in_outputIndex = 51;
        int in_outputStep = outputStep;
        int in_epochLimit = 200;
                
        /*
        Differential Evolution Parameters
         */                
        float in_beta = 1e-5f;
                
        /*
        Computational Evolution Parameters
        */                
        int in_populationSize = 20;
        int in_populationMax = 40; 
        float in_mutationRate = .2f;
        float in_crossoverRate = .1f;
        int in_mu = 4;
        int in_lambda = 2;
                
        /*
        Gaussian Parameters
        */                
        float in_radius = .5f; 
        float in_center = 0f;
                
        /*
        Backpropagation Parameters:
        */                
        float in_eta = 1.2e-7f;//0.0000006/200000.0;
        float in_momentum = 1e-9f;///200000.0;
        float in_exponent = 1.0f;
                
        /*
        Test Data Generation Parameters
        */                
        int in_dataSetSize = 200;
        int in_dimensionality = 51;
        float in_xValUpperBound = -1;
        float in_xValLowerBound = 0;              
        int dataSetSize = 6000;
        int dimensionality = 50;
        float xValLowerBound = -1.0f;
        float xValUpperBound = -1.0f;
                
        /*
        Weight Initialization Parameters
        */                
        float in_upperBoundWeight = 10.0f;
        float in_upperBoundBiasWeight = 1f;
                
        /*
        Network Parameters
         */       
        //int outputSize = 1;
        int[] in_hiddenLayers = {40}; //not used
        AbstractFunction[] in_activationFunction = {new Sigmoid(), new Sigmoid()}; //ReLU causes NaN often
        AbstractLoss in_lossFunction = lossFunction;
        //TrainingMethodInterface in_trainingMethod = new Backpropagation();

        
        /*
        K Folds Cross Validation Parameters
        */
        int in_repetitions = 5;
        

        
                        
        /*
        Input/Output Parameters
        */       
        


                
        //DataPackInterface in_data = new Read(airfoil, exclude_airfoil);
        //learnIndices = exclude_concrete;
        //DataPackInterface in_data = new Read(concrete, learnIndices, false, 0);
        DataPackInterface in_data = new Read(inputFile1, inputFile2, learnIndices, isClassification, yNum);

        
        in_xDataSet = in_data.initializeXDataSet(dataSetSize, dimensionality, xValLowerBound, xValUpperBound);
        in_dataSetSize = in_xDataSet.size();
        //System.out.println("Number of xdata elements is: " + in_xDataSet.size() * in_xDataSet.)
        in_yDataSet = in_data.initializeYDataSet(in_xDataSet, 0);
        rawData = new Data(in_xDataSet, in_yDataSet);
        //ArrayList<Double> in_input = in_data
        
        in_dataPair =  rawData;// = new NaiveFilter().filter(rawData);
        //in_outputSize = 3;
        //Read R = new Read("F:\\Google Drive\\github stuff\\MSUMLSheppard2015\\UCI_data\\airfoil\\use\\airfoil_self_noise.dat");
        //ArrayList<ArrayList<String>> airfoil = R.readToInput();
        
        /*int[] hiddenEncoderLayers = {50,20,50};
        Driver AutoEncoder = new Driver(
            in_xDataSet, in_xDataSet, in_dataPair,
            in_testPercentage, in_outputIndex, in_outputStep, in_epochLimit, 
            in_beta,
            in_mu, in_lambda,
            in_radius, in_center,
            1e-7, 1e-1, in_exponent,
            in_dataSetSize, in_dimensionality, in_xValUpperBound, in_xValLowerBound,
            in_upperBoundWeight, in_upperBoundBiasWeight,
            hiddenEncoderLayers, in_activationFunction, in_trainingMethod,
            in_outputSize,
            in_data,
            in_k, in_repetitions,
            in_isClassify, in_numClasses);
        
        AutoEncoder.run();
        
        Matrix M = AutoEncoder.outputLayer;*/
                
        
        ArrayList<Float> etaList = new ArrayList<>();//{1.2e-7, 1.3e-7, 1.4e-7, 1.4e-8, 1.4e-9};
        ArrayList<Float> momentList = new ArrayList<>();
        ArrayList<int[]> layersList = new ArrayList<>();
        
        for (int i = 0 ; i < 100; i++) {
            //double range = 1e-5-1e-15;
            float val = new Random().nextFloat()*etaRange;
            float center = val;// + 1e-7;
            etaList.add(val);
            //etaList.add(1e-7+(1e-5)*i);
        }
        
        for (int i = 0; i < 100; i++) {
            //double range = 8.0e-2;
            float val = new Random().nextFloat()*momentRange;
            //double center = val + 9e-1;
            momentList.add(val);
            //momentList.add(1e-5+(1*i)e-5)*;
        }
        
        for (int i = 0; i < 100; i++) {
            int val = new Random().nextInt(rangeNumLayers);
            int[] layer = new int[val+2]; // 2 hidden layers minimum
            
            
            
            for (int j = 0; j < layer.length; j ++ ) {
                int size = new Random().nextInt(rangeLayerSize);
                layer[j] = size + 20;
            }           
            layersList.add(layer);
        }
        
        autoExperimentRun(etaList, momentList, layersList,
                testPercentage, in_outputIndex, in_outputStep, in_epochLimit, 
                in_beta,
                in_mu, in_lambda,
                in_radius, in_center,
                in_eta, in_momentum, in_exponent,
                in_dataSetSize, in_dimensionality, in_xValUpperBound, in_xValLowerBound,
                in_upperBoundWeight, in_upperBoundBiasWeight,
                in_hiddenLayers, in_activationFunction, in_trainingMethod, in_lossFunction,
                in_outputSize,
                in_data,
                in_k, in_repetitions,
                isClassification, yNum,
                in_populationSize, in_populationMax,
                in_crossoverRate, in_mutationRate);


    }
    
    public static void autoExperimentRun(ArrayList<Float> etaList, ArrayList<Float> momentList, ArrayList<int[]> layersList,
        float in_testPercentage, int in_outputIndex, int in_outputStep, int in_epochLimit, 
        float in_beta,
        int in_mu, int in_lambda,
        float in_radius, float in_center,
        float in_eta, float in_momentum, float in_exponent,
        int in_dataSetSize, int in_dimensionality, float in_xValUpperBound, float in_xValLowerBound,
        float in_upperBoundWeight, float in_upperBoundBiasWeight,
        int[] in_hiddenLayers, AbstractFunction[] in_activationFunction, TrainingMethodInterface in_trainingMethod, AbstractLoss in_lossFunction,
        int in_outputSize,
        DataPackInterface in_data,
        int in_k, int in_repetitions,
        boolean in_isClassify, int in_numClasses,
        int in_populationSize, int in_populationMax,
        float in_crossoverRate, float in_mutationRate
         ) throws FileNotFoundException, IOException {

        System.setOut(console);
        //System.setOut(out);
        System.out.print("\nTEST ETA VALS: ");
        for (double eta : etaList) {
            System.out.print(eta + " ");
        }
        System.out.print("\nTEST MOMENTUM VALS: " );
        for (double moment : momentList) {
            System.out.print(moment + " ");
        }
        System.out.print("\nLAYER TOPOLOGY VALS: ");
        System.out.print(" { ");
        System.out.print( "");
        for (int[] layers : layersList ) {
            for(int i : layers ) {
                System.out.print(" " + i + " ");
            }
        }
        System.out.print(" }   ");
        
        System.out.println("");
        int i = 0;

        for (int c = 0; i < numTrials; i++) {
            int randEtaListIndex = new Random().nextInt(etaList.size());
            in_eta = etaList.get(randEtaListIndex);

            int randMomentListIndex = new Random().nextInt(momentList.size());               
            in_momentum = momentList.get(randMomentListIndex);

            int randLayerListIndex = new Random().nextInt(layersList.size());
            in_hiddenLayers = layersList.get(randLayerListIndex);


            Driver D = new Driver(
            in_xDataSet, 
            in_yDataSet, 
            in_dataPair,
            in_testPercentage, 
            in_outputIndex, 
            in_outputStep, 
            in_epochLimit, 
            in_beta,
            in_mu, 
            in_lambda,
            in_radius, 
            in_center,
            in_eta, 
            in_momentum, 
            in_exponent,
            in_dataSetSize, 
            in_dimensionality, 
            in_xValUpperBound, 
            in_xValLowerBound,
            in_upperBoundWeight, 
            in_upperBoundBiasWeight,
            in_hiddenLayers, 
            in_activationFunction, 
            in_trainingMethod,
            in_lossFunction,
            in_outputSize,
            in_data,
            in_k, 
            in_repetitions,
            in_isClassify, 
            in_numClasses,
            in_populationSize, 
            in_populationMax,
            in_crossoverRate, 
            in_mutationRate);
            DriverPopulation.add(D);
        }
        //D.trainingMethod = new Backpropagation();

        System.out.println("\n------- NEW TRIAL ------- ");

        System.out.format("RUNNING WITH ETA: " + in_eta + "%n");
        System.out.format("RUNNING WITH MOMENTUM: " + in_momentum + "%n");
        System.out.format("%nRUNNING WITH LAYER TOPOLOGY: ");
        System.out.print("{ ");
        for (int layer : in_hiddenLayers ) {
            System.out.print(" " + layer + " ");

        }


        System.out.print(" } ");
        //DriverPopulation.get(0).nNetHelper.runWithOutput();
        DriverIterator = DriverPopulation.iterator();
        while (DriverIterator.hasNext()) {
            try {  
                RunNew();
            } catch (InterruptedException ex) {
                System.out.println("INTERRUPTED EXCEPTION");
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }       
        System.out.println("\n------- END TRIAL " + i + " -------");
        i++;
        //D.clear();
        //System.out.println("END TRIAL" + i + ":");   
        System.gc();
            //}
        //}

        //}


        System.out.println("------- INITIALIZATION DONE! ------- ");
        System.out.flush();
        
        E.shutdown();
        if(E.isTerminated()) {
            parse(tracker);
        }
    }

    public static void RunNew() throws FileNotFoundException, IOException, InterruptedException {
        if (DriverIterator.hasNext()) {
            Driver driver = DriverIterator.next();
 
            System.out.println("\n------- NEW TRIAL ------- ");

            //System.out.print(" } ");
            System.out.flush();
            
            if (numThreads == 1) {
                driver.netDriver.runWithOutput();
            } else {
            
            
            Runnable R = new DriverThread(driver);
            E.submit(R);
            }
            //E.execute(R);
            trialCounter++;
            System.gc();
            }
        //E.shutdown();
        //System.exit(0);
    }
    
    public static synchronized void addTracker(MatrixNeuralNetTracker in) {
        tracker.add(in);
    }

    private static void parse(ArrayList<MatrixNeuralNetTracker> tracker) {
        
        
        for (MatrixNeuralNetTracker t : tracker) {
            
        }
    }
    
}


