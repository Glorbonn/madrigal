package DriverPack;

import Math.Matrix;
import java.math.BigDecimal;
import java.util.ArrayList;
import javafx.util.Pair;

/**
 * MiniLib contains miscellaneous features like shuffling, getting values of 
 * pair lists.
 * @author Ross Wendt
 */
public class MiniLib {

    public static Matrix[] getValues(Pair<Matrix, Matrix>[] in) {
        Matrix[] ret = new Matrix[in.length];
        for (int i = 0; i < in.length; i++) {
            ret[i] = in[i].getValue();
        }
        return ret;
    }

    //             https://www.youtube.com/watch?v=sVzvRsl4rEM gud saong brouh
    public static Matrix[] getKeys(Pair<Matrix, Matrix>[] in) {
        Matrix[] ret = new Matrix[in.length];
        for (int i = 0; i < in.length; i++) {
            ret[i] = in[i].getKey();
        }
        return ret;
    }

    public static Pair<double[], double[]>[] shuffle(Pair<double[], double[]>[] shuffled) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /*
    Find and return max for input 2d double array
    */
    public static float findMax(float[][] in) {
        float max = -Float.MAX_VALUE;
        
        for (int i = 0; i < in.length; i++) {
            for (int j = 0; j < in [0].length; j++) {
                if (in[i][j] > max ) {
                    max = in[i][j];//in[i][j] = max;
                }
            }
        }
        return max;
    }
    
    /*
    Find and return max for a 2D list
    */
    public static float findMax(ArrayList<ArrayList<Float>> in) {
        float max = -Float.MAX_VALUE;
        
        for (int i = 0; i < in.size(); i++) {
            for (int j = 0; j < in.get(0).size(); j++) {
                if (in.get(i).get(j) > max ) {
                    max = in.get(i).get(j);//in[i][j] = max;
                }
            }
        }
        return max;
    }
    
    /*
    Find and return min for input 2d double array
    */
    public static float findMin(float[][] in) {
        float min = Float.MAX_VALUE;
        for (int i = 0; i < in.length; i++) {
            for (int j = 0; j < in[0].length; j++) {
                if (in[i][j] < min ) {
                    min = in[i][j];
                    //in[i][j] = min;
                }
            }
        }
        return min;
    }
    
    /*
    Find and return min for input 2d list
    */
    public static float findMin(ArrayList<ArrayList<Float>> in) {
        float min = Float.MAX_VALUE;
        for (int i = 0; i < in.size(); i++) {
            for (int j = 0; j < in.get(0).size(); j++) {
                if (in.get(i).get(j) < min ) {
                    min = in.get(i).get(j);
                    //in[i][j] = min;
                }
            }
        }
        return min;
    }

    public static ArrayList<Matrix> getValues(ArrayList<Pair<Matrix, Matrix>> in) {
        ArrayList<Matrix> ret = new ArrayList<>();
        for (int i = 0; i < in.size(); i++) {
            ret.add(in.get(i).getValue());
        }
        return ret;
    }

    public static ArrayList<Matrix> getKeys(ArrayList<Pair<Matrix, Matrix>> in) {
        ArrayList<Matrix> ret = new ArrayList<>();
        for (int i = 0; i < in.size(); i++) {
            ret.add(in.get(i).getKey());
        }
        return ret;
    }
    
    /*
    Calculate mean error from a matrix
    */
    public static float calcError(Matrix in) {
        float error = 0;
        for (int i = 0; i < in.getRows(); i++) {
            for (int j = 0; j < in.getColumns(); j++) {
                error += in.getArray()[i][j];
            }
        }
        
        error /= in.getArray().length;
        //error = Math.pow(error, 0.5);

        return error;
    }
    
    /*
    Calculate mean error from a list
    */
    public static double calcListError(ArrayList<Double> errorList) {
        double error = 0;
        for (int i = 0; i < errorList.size(); i++) {
            error += errorList.get(i);
        }
        error /= errorList.size();
        //error = Math.pow(error, 0.5);
        return error;
    }
    
    
    public static double[] softMax(float[] inputs) {
        double[] classProbability = new double[inputs.length];
        double[] shiftedInput = new double[inputs.length];
        double max = -Double.MAX_VALUE;
        // normalize by shifting the values such that 0 is the highest
        
        // Find max value
        for (int i = 0; i < inputs.length; i++ ) {
            if (inputs[i] >= max) {
                max = inputs[i];
            }
        }
        
        // Subtract max value
        
        for (int i = 0; i < shiftedInput.length; i++) {
            shiftedInput[i] = inputs[i] - max;
        }
        
        double sum = 0;
        for (int i = 0; i < shiftedInput.length; i++ ) {
           sum += Math.exp(shiftedInput[i]);
        }
        
        for (int i = 0; i < shiftedInput.length; i++) {

            classProbability[i] = Math.exp(shiftedInput[i]) / sum;
        }
        
        return classProbability;    
    }
    
    /*
    Take 
    */
    public static boolean isClass(String inputClassAsString, int numClasses, int[] classes) {
        //if (s.contains(".")) {
        //    return false;
        //}
        /*
        Check to see if a value read in as a string is a valid class
         */
        for (int i = 0; i < numClasses; i++) {
            try {
                if (new BigDecimal(inputClassAsString).intValue() == classes[i]) {
                    return true;
                }
            } catch(NumberFormatException e) {
                return false;
            }
        }
        return false;
    }

    /*
    Use argmax to find whether target output of net agrees with actual output of net
     */
    public static boolean isCorrect(float[] actual, float[] predicted) {
        //for (int i = 0; i < actual.length; i++) {
        //    System.out.print(actual[i] + " ");
        //}
        //System.out.println("");
        

        int actualClass = findActualClass(actual);
        int findPredictedClass = findPredictedClass(predicted);
        //System.out.println("Predicted class is " + findPredictedClass);
        return actualClass == findPredictedClass; //System.out.println("Classes match.");
        //System.out.println("Classes do not match.");
    }
    /*
    Actual classes are one-hot encoded and for our data examples are only a member
    of one class.
    */
    private static int findActualClass(float[] oneHotActualClass) {
        int classReturn = 0;
        for (int i = 0; i < oneHotActualClass.length; i++) {
            if ( oneHotActualClass[i] ==1 ) {
                classReturn = i;
                break;
            }
        }
        return classReturn;
    }
    
    /*
    Use softmax to find the predicted class.
    */
    public static int findPredictedClass(float[] oneHotPredictedClass) {
        int classReturn = 0;
        double max = -Double.MAX_VALUE;        
        double[] softMaxOneHot =  softMax(oneHotPredictedClass);
        
        for (int i = 0; i < softMaxOneHot.length; i++) {
            if (softMaxOneHot[i] >= max) {
                max = softMaxOneHot[i];
                classReturn = i;
            }
        }
        
        return classReturn;
        
        
    }

    /*
    The function assumes neural net outputs values closest to 1 being the 
    predicted class.
    */
    private static int findClass(float[] in) {
        int potentialClass = Integer.MAX_VALUE;
        double min = Integer.MAX_VALUE;
        float[] error = new float[in.length];
        /*
        Calculate error from expected class (which is 1 in some index of in)
         */
        for (int i = 0; i < in.length; i++) {
            error[i] = 1.0F - in[i];
        }
        /*
        Find index (class) of absolute minimum value in error
         */
        for (int i = 0; i < in.length; i++) {
            if (Math.abs(error[i]) < min) {
                min = Math.abs(error[i]);
                potentialClass = i;
            }
        }
        return potentialClass;
    }
    
    
}
