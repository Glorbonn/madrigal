package DataPack;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Ross Wendt
 */
public abstract class GenerateValues extends DataPackInterface{

    /**
     *
     * @param samples
     * @param n
     * @param lowerBound
     * @param upperBound
     * @param xData
     * @param colNumber
     * @return
     */
    @Override
    public Data makeData(int samples, int n, float lowerBound, float upperBound, ArrayList<ArrayList<Float>> xData, int colNumber) {
        xData = initializeXDataSet(samples, n, lowerBound, upperBound);
        ArrayList<ArrayList<Float>> yData = initializeYDataSet(xData, colNumber);
        Data dataPair = new Data(xData, yData);
        return dataPair;
    }

    /**
     *
     * @param xDataSet
     * @param colNumber
     * @return
     */
    @Override
    public ArrayList<ArrayList<Float>>  initializeYDataSet(ArrayList<ArrayList<Float>> xDataSet, int colNumber) {
        ArrayList<ArrayList<Float>>  yDataSet = new ArrayList<>();
        for (ArrayList<Float> D : xDataSet) {
            ArrayList<Float> helper = new ArrayList<>();
            float d = computeFunction(D);
            helper.add(d);
            //ArrayList<float> helper = new ArrayList<>();
            //for (float d : D) {
            //    helper.add(computeFunction(d));
            //}
            yDataSet.add(helper);
        }
        return yDataSet;
    }

    /**
     *
     * @param x
     * @return
     */
    @Override
    public abstract float computeFunction(ArrayList<Float> x);

    /**
     *
     * @param samples
     * @param n
     * @param lowerBound
     * @param upperBound
     * @return
     */
    @Override
    public ArrayList<ArrayList<Float>> initializeXDataSet(int samples, int n, float lowerBound, float upperBound) {
        Random rdm = new Random();
        ArrayList<ArrayList<Float>> xDataSet = new ArrayList<>(); //new double[samples][n];

        
        for (int i = 0; i < samples; i++) {
            ArrayList<Float> randSet = new ArrayList<Float>();
            for (int j = 0; j < n; j++) {
                Float randNum = rdm.nextFloat() * (upperBound - lowerBound) + lowerBound;
                randSet.add(randNum);
            }
            xDataSet.add(randSet);
        }
        return xDataSet;
    }
    
}
