package DataPack;
import DriverPack.Controller;
import DriverPack.MiniLib;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Implements the data interface by reading CSV files into the program.
 * 
 * @author Ross Wendt
 */
public class Read extends DataPackInterface {
    int numClasses;
    boolean validClass;
    boolean path2Exists;
    int[] indexedClasses;
    File in;
    File in2;
    List<Integer> learnIndices;
    ArrayList<ArrayList<Float>> readXVals = new ArrayList<>();
    ArrayList<ArrayList<Float>> readYVals = new ArrayList<>();

    /**
     *
     * @param path
     * @param path2
     * @param learnIndices
     * @param validClass
     * @param numClasses
     * @throws IOException
     */
    public Read(String path, String path2, List<Integer> learnIndices, boolean validClass, int numClasses) throws IOException {
        in = new File(path);       
        if (path2 == null)     
            path2Exists = false;
        else { 
            path2Exists = true;
            in2 = new File(path2);            
        }
        this.learnIndices = learnIndices;
        this.validClass = validClass;
        this.numClasses = numClasses;
        
        indexedClasses = new int[numClasses];
        for (int i = 0; i < numClasses; i++) {
            indexedClasses[i] = i;
        }
    }
    
    /*
    Runs driver method for either single file input or multi file input.
    */   

    /**
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
   
    public void readDriver() throws FileNotFoundException, IOException {
        if (path2Exists) {
            multiInputDriver();
        } else {
            singleInputDriver();
        }
    }
    
    /*
    Reads file into a list of <Double> lists.
    */

    /**
     *
     * @param reader
     * @param path
     * @param delim
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */

    public ArrayList<ArrayList<Float>> readMulti(BufferedReader reader, Path path, String delim) throws FileNotFoundException, IOException {
        String line = null;
        ArrayList<ArrayList<Float>> readValues = new ArrayList<>();


        while ((line = reader.readLine()) != null) {
            String[] read = line.split(delim);
                ArrayList<Float> readSingleLine = new ArrayList<>();
                for (String s : read) {
                    if (!s.contains("-") && !s.equals("")) {
                        if (MiniLib.isClass(s, numClasses, indexedClasses)) {
                            //double[] helperArray = new double[numClasses];
                            /*
                            Do one-hot encoding with doubles
                            */
                            for (int i = 0; i < numClasses; i++) {
                                if (Integer.parseInt(s) == i ) {
                                    readSingleLine.add(1.0f);
                                } else {
                                    readSingleLine.add(0.0f);
                                }
                            }
                        } else {
                            readSingleLine.add(Float.parseFloat(s));
                        }
                    }
                }
                readValues.add(readSingleLine);
                }
       reader.close();

       
       
       return readValues;
    }
    
    /*
    Calls read method for a single file.
    */

    /**
     *
     * @throws FileNotFoundException
     * @throws IOException
     */

    public void singleInputDriver() throws FileNotFoundException, IOException {
        Path path1 = in.toPath();
        final String delim = ",";
        System.out.println(new File(".").getAbsoluteFile());
        System.out.println("Working Directory = " +
              System.getProperty("user.dir"));
        BufferedReader reader = Files.newBufferedReader(path1);
        ArrayList<ArrayList<ArrayList<Float>>> readerHelper = readSingle(reader, delim);
        readXVals = readerHelper.get(0);
        readYVals = readerHelper.get(1);
    }
    
    /*
    Calls read method for multiple files.
    */    

    /**
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    
    public void multiInputDriver() throws FileNotFoundException, IOException {
        //readXVals = new ArrayList<>();
        Path path1 = in.toPath();
        Path path2 = in2.toPath();
        final String delim = ",";
        BufferedReader reader = Files.newBufferedReader(path1);
        BufferedReader reader2 = Files.newBufferedReader(path2);
        readXVals = readMulti(reader, path1, delim);
        readYVals = readMulti(reader2, path2, delim);
        /*
        Set YVals to list with classes by swapping readXVals and readYVals
        */
        
        if (wrongOrder(readXVals, readYVals)) {
            ArrayList<ArrayList<Float>> temp = readYVals;
            readYVals = readXVals;
            readXVals = temp;      
        }
        
        
        System.out.println("DONE READING");
    }
    
    private boolean wrongOrder(ArrayList<ArrayList<Float>> x, ArrayList<ArrayList<Float>> y) {
        if (x.get(0).size() == numClasses ) {
            return true;
        } else {
            return false;
        }
    }
    
    /*
    Reads a single file into a two parts: a list for the X data (examples), 
    and Y data (learning data). returned list [0] is the X data, [1] is the Y.
    */

    /**
     *
     * @param reader
     * @param delim
     * @return
     * @throws IOException
     */

    public ArrayList<ArrayList<ArrayList<Float>>> readSingle(BufferedReader reader, String delim) throws IOException {
        String line = null;
        ArrayList<ArrayList<ArrayList<Float>>> returnTwo = new ArrayList<>();
        ArrayList<ArrayList<Float>> readX = new ArrayList<>();
        ArrayList<ArrayList<Float>> readY = new ArrayList<>();
        int valueCounter = 0; //index
        
        while ((line = reader.readLine()) != null) {
            String[] read = line.split(delim);
                ArrayList<Float> readSingleLineXVals = new ArrayList<>();
                ArrayList<Float> readSingleLineYVals = new ArrayList<>();
                for (String s : read) {
                    if (!s.contains("-") && !s.equals("")) {
                        if (Controller.learnIndices.contains(valueCounter) ) {
                            if (MiniLib.isClass(s, numClasses, indexedClasses)) {
                                //double[] helperArray = new double[numClasses];
                                /*
                                Do one-hot encoding with doubles
                                */
                                for (int i = 0; i < numClasses; i++) {
                                    if (Integer.parseInt(s) == i ) {
                                        readSingleLineYVals.add(1.0f);
                                    } else {
                                        readSingleLineYVals.add(0.0f);
                                    }
                                }
                            } else {
                                readSingleLineYVals.add(Float.parseFloat(s));
                            }
                        } else {
                            readSingleLineXVals.add(Float.parseFloat(s));
                        }
                    }
                    valueCounter++;
                }
                valueCounter = 0;
                //readValues.add(readSingleLine);
                readX.add(readSingleLineXVals);
                readY.add(readSingleLineYVals);
                }
        reader.close();
        
        returnTwo.add(readX);
        returnTwo.add(readY);
        
        return returnTwo;
        
    }
    
    /*
    Creates a Data pair to keep each X with its related Y.
    */

    /**
     *
     * @param read
     * @param colNumber
     * @return
     */

    public Data makeData(ArrayList<ArrayList<Float>> read, int colNumber) {
        
        ArrayList<ArrayList<Float>> xData = initializeXDataSet(1,2,3,4);
        ArrayList<ArrayList<Float>> yData = initializeYDataSet(read, colNumber);
        Data D = new Data(xData, yData);
        return D;
    }

    /*
    NYI:  data generation
    */

    /**
     *
     * @param samples
     * @param n
     * @param lowerBound
     * @param upperBound
     * @param xDataSet
     * @param colNumber
     * @return
     */

    public Data makeData(int samples, int n, float lowerBound, float upperBound, float[][] xDataSet, int colNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param x
     * @return
     */
    @Override
    public float computeFunction(ArrayList<Float> x) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     *
     * @param notUsed
     * @param notUsed2
     * @return
     */
    @Override
    public ArrayList<ArrayList<Float>> initializeYDataSet(ArrayList<ArrayList<Float>> notUsed, int notUsed2) {
        return readYVals;
    }

    /**
     *
     * @param notUsed
     * @param notUsed2
     * @param notUsed3
     * @param notUsed4
     * @return
     */
    @Override
    public ArrayList<ArrayList<Float>> initializeXDataSet(int notUsed, int notUsed2, float notUsed3, float notUsed4) {
        try {
            readDriver();
        } catch (IOException ex) {
            Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
        }
        return readXVals;
    }
}
