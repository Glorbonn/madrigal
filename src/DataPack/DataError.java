/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataPack;

/**
 * DataError is a class that tracks, for classification problems, the number of
 * correctly classified data and the number of incorrectly classified data.
 * 
 * @author rossw
 */
public class DataError {
    private int correct, wrong;
    private float error;

    /**
     *
     * @param correct number of correctly classified data
     * @param wrong the number of incorrectly classified data
     * @param error the percentage of incorrectly classified data
     */
    public DataError(int correct, int wrong, float error) {
        this.correct = correct;
        this.wrong = wrong;
        this.error = error;
    }
    
    /**
     *
     */
    public DataError() {
        this.correct = 0;
        this.wrong = 0;
        this.error = 0.0f;
    }
    
    /**
     *
     * @return wrong
     */
    public int getWrong() {
        return this.wrong;
    }
    
    /**
     *
     * @return correct
     */
    public int getCorrect() {
        return this.correct;
    }
    
    /**
     *
     * @return error
     */
    public float getError() {
        return this.error;
    }
    
    /**
     * 
     * 
     * @param in the integer weight of the data (1 normally)
     */
    public void accumulateCorrect(int in) {
        this.correct += in;
    }
    
    /**
     *
     * @param in
     */
    public void accumulateWrong(int in) {
        this.wrong += in;
    }
    
    /**
     *
     * @param in
     */
    public void accumulateError(double in) {
        this.error += in;
    }
}
