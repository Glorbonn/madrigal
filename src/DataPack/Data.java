package DataPack;

import DriverPack.Controller;
import DriverPack.MiniLib;
import Math.Matrix;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import javafx.util.Pair;

/**
 * Data is a class that pairs x, y data together as pairs, and partitions 
 * shuffled data into test, train, and validation sets.
 * 
 * @author Ross Wendt
 */
public class Data {
    private ArrayList<ArrayList<Float>> xDataList;
    private ArrayList<ArrayList<Float>> yDataList;
    ArrayList<Pair<ArrayList<Float>,ArrayList<Float>>> xyDataPairs;
    private static ArrayList<ArrayList<Pair<Matrix, Matrix>>> test;
    private static ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>> train;
    private static ArrayList<Pair<ArrayList<Float>, ArrayList<Float>>> shuffled;

    /**
     *
     * @param in_xDataSet the data fed to the input layer of the network
     * @param in_yDataSet the data fed to the output layer of the network
     */
    public Data(ArrayList<ArrayList<Float>> in_xDataSet, ArrayList<ArrayList<Float>>  in_yDataSet) {
        xyDataPairs = new ArrayList<>();
        xDataList = normalize(in_xDataSet);
        //if(Controller.isClassification) {
        //    yDataList = onehot(in_yDataSet);
        //} else {
            yDataList=normalize(in_yDataSet);
        //}
        test = new ArrayList<>();
        train = new ArrayList<>();
        //validation = new ArrayList<>(); //this is the k-th subset left out specified in neuralNetDriver
        
        for (int i = 0; i < in_xDataSet.size(); i++) {
            xyDataPairs.add(new Pair(in_xDataSet.get(i), in_yDataSet.get(i)));
        }
        System.out.println("MAPPING DONE");

        shuffled = xyDataPairs;
        Collections.shuffle(xyDataPairs);
        
        //validation = partitionValidation(xyDataPairs);
        test = partitionTest();
        train = partitionTrain();
        
        if (Controller.windowFlag == true ) {
            test = testToSlidingWindow(test, Controller.windowSize);
            train = trainToSlidingWindow(train, Controller.windowSize);
        }
    }
    
    /**
     * @return the xData
     */
    public ArrayList<ArrayList<Float>> getXData() {
        return xDataList;
    }

    /**
     * @param xData the xData to set
     */
    public void setXData(ArrayList<ArrayList<Float>> xData) {
        this.xDataList = xData;
    }

    /**
     * @return the yData
     */
    public ArrayList<ArrayList<Float>> getYData() {
        return yDataList;
    }

    /**
     * @param yData the yData to set
     */
    public void setYData(ArrayList<ArrayList<Float>> yData) {
        this.yDataList = yData;
    }
    
    /**
     *
     * @return the x, y paired data as a HashMap (NYI)
     */
    public ArrayList<Pair<ArrayList<Float>, ArrayList<Float>>> getHashMap() {
        return xyDataPairs;
    }
    
    /**
     * Getter for shuffled data
     *
     * @return the shuffled data
     */
    public ArrayList<Pair<ArrayList<Float>, ArrayList<Float>>> getShuffledSet() {
        return shuffled;
    }
    
    /*

    */

    /**
     * 
     * Segments test data into relevant number of lists of minibatch size,
     * together in a single list of unwrapped size testPercentage * dataSize.
     * 
     * This method should be rewritten to take as inputs minibatch and 
     * testPercentage from Controller.
     *
     * @return the test data partitioned 
     */

    public final ArrayList<ArrayList<Pair<Matrix, Matrix>>> partitionTest() {
        ArrayList<ArrayList<Pair<Matrix, Matrix>>> out = new ArrayList<>();
        int nullCounter = 0;
        ArrayList<Pair<Matrix, Matrix>> testHelper = new ArrayList<>();
        for (int i = 0; i < Math.floor(shuffled.size() * Controller.in_testPercentage); i++) {
            testHelper.add(new Pair(new Matrix(shuffled.get(i).getKey()), new Matrix(shuffled.get(i).getValue())));
            shuffled.set(i, null);
            nullCounter++;
        }
        System.out.println("\n\n\n " + nullCounter + " \n\n\n");
        Iterator I = shuffled.iterator();
        while (I.hasNext()) {
            if (I.next() == null) {
                I.remove();
            }
        }
        int batches = testHelper.size() / Controller.minibatch;
        Iterator it = testHelper.iterator();
        for (int i = 0; i < batches; i++) {
            ArrayList<Pair<Matrix,Matrix>> batch = new ArrayList<>();
            for (int b = 0; b < Controller.minibatch; b++) {
                if (it.hasNext())
                batch.add((Pair)it.next());
            }
            out.add(batch);
        }
        
        return out;
    }

    /*

    */

    /**
     * Segments train data into appropriate k-subsets and each kth subset into 
     * appropriate number of minibatches.
     * 
     * @return the partitioned training data
     */

    public final ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>> partitionTrain() {
        ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>> kSubsets = new ArrayList<>();
        int batches = shuffled.size() / Controller.in_k / Controller.minibatch;
        Iterator it = shuffled.iterator();
        for (int kthSubset = 1; kthSubset <= Controller.in_k; kthSubset++) {
            ArrayList<ArrayList<Pair<Matrix,Matrix>>> subset = new ArrayList<>();
            for (int i = 0; i < batches; i++) {
                ArrayList<Pair<Matrix,Matrix>> batch = new ArrayList<>();
                for (int b = 0; b < Controller.minibatch; b++ ) {
                    Pair val = (Pair) it.next();
                    Matrix key = new Matrix((ArrayList<Float>) val.getKey());
                    Matrix value = new Matrix((ArrayList<Float>) val.getValue());
                    batch.add(new Pair(key , value));
                }
                subset.add(batch);
            }
            kSubsets.add(subset);
        }

        return kSubsets;
    }   
    

    /**
     * Removes an attribute for use in NYI feature selection/ extraction.
     * 
     * @param index the integer index of the data (which is a column of data)
     * @return the new data without the specified index
     */

    public Data removeAttribute(ArrayList<Integer> index) {
        ArrayList<ArrayList<Float>> newXData = new ArrayList<>();

        for (int i = 0; i < this.xDataList.size(); i++) {        
            ArrayList<Float> dataValues = new ArrayList<>();
            for (int j = 0; j < xDataList.get(0).size(); j++) {
                if (!index.contains(j)) {
                   dataValues.add(this.xDataList.get(i).get(j));
                }
                newXData.add(dataValues);
            }
        }
        
        return new Data(newXData, this.yDataList);
    }
    
    private ArrayList<ArrayList<Float>> onehot(ArrayList<ArrayList<Float>> in_yDataSet) {
        return in_yDataSet;
    }
    
    /**
     * Squishes data between 0 and 1. 
     * 
     * @param in_xDataSet the data to be squished
     */
    private ArrayList<ArrayList<Float>> normalize(ArrayList<ArrayList<Float>> in_xDataSet) {
        float max = MiniLib.findMax(in_xDataSet);
        float min = MiniLib.findMin(in_xDataSet);
        
        for (int i = 0; i < in_xDataSet.size(); i++) {
            for (int j = 0; j < in_xDataSet.get(0).size(); j++ ) {
                                
                float val = in_xDataSet.get(i).get(j);
                val = (val - min) / (max - min);
                in_xDataSet.get(i).set(j, val);                
            }
        }
        
        return in_xDataSet;
    }
    
    /**
     *
     * @return the train data
     */
    public static ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>> getTrain() {
        return Data.train;
    }
    
    /**
     *
     * @return the test data
     */
    public static ArrayList<ArrayList<Pair<Matrix, Matrix>>> getTest() {
        return Data.test;
    }
    
    /**
     * Takes test data and transforms it into a list of data with sliding
     * windows. NYI. Note: It is better to slide the window when reading data,
     * instead of duplicating data into sliding windows.
     * 
     * @param in the input test data
     * @param windowSize the size of the sliding window
     * @return
     */
    public static ArrayList<ArrayList<Pair<Matrix, Matrix>>> testToSlidingWindow(ArrayList<ArrayList<Pair<Matrix, Matrix>>> in, int windowSize) {
        ArrayList<ArrayList<Pair<Matrix, Matrix>>> out = new ArrayList<>();
        return out;
    }
    
    /**
     * Takes train data and transforms it into a list of data with sliding
     * windows. NYI. Note: It is better to slide the window when reading data,
     * instead of duplicating data into sliding windows.
     * @param in the input training data
     * @param windowSize the size of the sliding window
     * @return
     */
    public static ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>> trainToSlidingWindow(ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>> in, int windowSize) {
        ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>> out = new ArrayList<>();
        return out;
    }
}
