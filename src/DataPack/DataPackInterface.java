package DataPack;

import java.util.ArrayList;

/**
 *
 * @author Ross Wendt
 */
public abstract class DataPackInterface {
    ArrayList<ArrayList<Float>> xData = new ArrayList<>();
    ArrayList<ArrayList<Float>> yData = new ArrayList<>();
    
    /**
     *
     * @param samples
     * @param n
     * @param lowerBound
     * @param upperBound
     * @param xDataSet
     * @param colNumber
     * @return
     */
    public Data makeData(int samples, int n, float lowerBound, float upperBound, ArrayList<ArrayList<Float>> xDataSet, int colNumber) {
        xData = initializeXDataSet(samples, n, lowerBound, upperBound);
        yData = initializeYDataSet(xData, colNumber);
        Data dataPair = new Data(xData, yData);
        return dataPair;
    }

    /**
     *
     * @param xDataSet
     * @param colNumber
     * @return
     */
    public abstract ArrayList<ArrayList<Float>> initializeYDataSet(ArrayList<ArrayList<Float>> xDataSet, int colNumber);
    
    /**
     *
     * @param x
     * @return
     */
    public abstract float computeFunction(ArrayList<Float> x);

    /**
     *
     * @param samples
     * @param n
     * @param lowerBound
     * @param upperBound
     * @return
     */
    public abstract ArrayList<ArrayList<Float>> initializeXDataSet(int samples, int n, float lowerBound, float upperBound);

}
