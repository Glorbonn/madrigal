package Math;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
/**
 *
 * @author Ross Wendt
 */
public class Matrix {
    private final int rows;
    private final int columns;
    private float[][] matrixValues;
    
    /*
    initialize a concatenated matrix from a matrix arraylist
    */
    
    public Matrix() {
        rows = 1;
        columns = 1;
        matrixValues = new float[1][1];
    }
    
    public Matrix(List<Matrix> initialMatrixValues) {
        rows = initialMatrixValues.size();
        columns = initialMatrixValues.get(0).getColumns();
        matrixValues = new float[rows][columns];
        for (int i = 0; i < rows; i++) {
            matrixValues[i] = initialMatrixValues.get(i).getArray()[0];
            //for (int j = 0; j < rows; j++) {
            //    matrixValues[j] = initialMatrixValues.get(i).getArray()[0];
            //}
        }
    }
    
    public Matrix(double[][] values) {
        rows = values.length;
        columns = values[0].length;
        matrixValues = new float[rows][columns];
        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < values[0].length; j++) {
                matrixValues[i][j] = (float) values[i][j];
            }
        }
    }
    
    /*
    initialize a matrix from a double list.
    */
    public Matrix(ArrayList<Float> initialMatrixValues) {
        rows = 1;
        columns = initialMatrixValues.size();
        matrixValues = new float[rows][columns];
        float[] d = new float[columns];
        for (int i = 0; i < columns; i++) {
            d[i] = initialMatrixValues.get(i);
        }
        matrixValues[0] = d;
        //matrixValues[0] = initialMatrixValues.toArray(new double[5]);
    }
    
    /*
    initializes a matrix based on a 2D double array
    */    
    public Matrix(float[][] initialMatrixValues){
        rows = initialMatrixValues.length;
        columns = initialMatrixValues[0].length;
        //matrixValues = new double[rows][columns];
        matrixValues = initialMatrixValues;
        //for(int i = 0; i < matrixValues.length; i++){
        //    System.arraycopy(initialMatrixValues[i], 0, matrixValues[i], 0, matrixValues[0].length);
        //}
    }
    /*
    initializes a Matrix based on 1D double array
    */
    public Matrix(float[] initialMatrixValues){
        rows = 1;
        columns = initialMatrixValues.length;
        matrixValues = new float[rows][columns];
        matrixValues[0] = initialMatrixValues;
        //System.arraycopy(initialMatrixValues, 0, matrixValues[0], 0, matrixValues[0].length);
    }   

    public Matrix(Map.Entry<ArrayList<Double>, ArrayList<Double>> get) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Matrix(float[][] initialMatrix, float val) {
        rows = initialMatrix[0].length;
        columns = initialMatrix.length;
        matrixValues = new float[rows][columns];
        for (int i = 0; i < initialMatrix[0].length; i++ ) {
            for (int j = 0; j < initialMatrix.length; j++) {
                matrixValues[i][j] = val;
            }
        }
    }

    public int getRows(){
        return rows;
    }   
    public int getColumns(){
        return columns;
    }   
    public float[][] getArray(){
        return matrixValues;
    }   
    public float[][] getMatrixValues(){
        return matrixValues;
    }
    public void setMatrixValues(int index1, int index2, float value) {
        matrixValues[index1][index2] = value;
    }
    /*
    matrix transposition
    */
    public Matrix transpose() {
        //if (this.getColumns() == this.getRows()) {
        //    System.out.println("YES");
        //}
        float[][] resultMatrix = new float[this.getColumns()][this.getRows()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        int rows = this.getRows();
        int columns = this.getColumns();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                resultMatrix[j][i] = this.getArray()[i][j];
            }
        }
        
        //System.out.println("In transpose ");
        //checkNaN(new Matrix(resultMatrix));
        return new Matrix(resultMatrix);
    }
    
    public Matrix copy() {
        float[][] zeroMatrix = new float[this.getRows()][this.getColumns()];
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getColumns(); j++) {
                zeroMatrix[i][j] = this.getArray()[i][j];
            }
        }
        //System.out.println("In copy ");
        //checkNaN(new Matrix(zeroMatrix));
        return new Matrix(zeroMatrix);
    }
    /*
    matrix addition
    */
    public Matrix madd(Matrix b) {
        //double[][] resultMatrix = new double[this.getRows()][this.getColumns()];
        
        if (this.getRows() != b.getRows() || this.getColumns() != b.getColumns()) {
            System.err.println("ERROR MATRIX MISMATCH MADD");
            System.out.println(this.getColumns() + " " + this.getRows() + "\n" + b.getColumns() + " " + b.getRows());
        }
        float[][] zeroMatrix = new float[this.getRows()][b.getColumns()];
        Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getColumns(); j++) {
                //this.getArray()[i][j] += b.getArray()[i][j];
                resultMatrix.getArray()[i][j] = this.getArray()[i][j] + b.getArray()[i][j];
            }
        }
        //System.out.println("In madd ");
        //checkNaN(resultMatrix);
        return resultMatrix;
    }
    /*
    adds bias value to all elements of target matrix
    */
    public Matrix addBiasMatrices(Matrix bias) {
        float[][] resultMatrix = new float[this.getRows()][this.getColumns()];
        //double[][] zeroMatrix = new double[this.getRows()][this.getColumns()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < bias.getColumns(); i++) {
            for (int j = 0; j < this.getRows(); j++) {
                for (int k = 0; k < this.getColumns(); k++) {
                    resultMatrix[j][k] = this.getArray()[j][k] + bias.getArray()[0][i];
                }
            }
        }
        //System.out.println("addBiasMatrices ");
        //checkNaN(new Matrix(resultMatrix));
        return new Matrix(resultMatrix);
    }
    /*
    randomly initializes matrix with upperbound
    */
    public Matrix init_rand(float upperBound) {
        //Random randomGenerator = new Random();
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                //double lowerBound = 0;
                float randomValue = (float) ThreadLocalRandom.current().nextDouble(-upperBound,upperBound);//lowerBound + (upperBound - lowerBound) * randomGenerator.nextDouble();
                this.getArray()[i][j] = randomValue;
            }
        }
        return this;
    }
    /*
    hadamard product, also called element-wise product
    */
    public Matrix hadProd(Matrix b) {
        //double[][] resultMatrix = new double[this.getRows()][this.getColumns()];
        
        if (this.getRows() != b.getRows() || this.getColumns() != b.getColumns()) {
            System.err.println("ERROR MATRIX MISMATCH HADAMARD");
            System.out.println(this.getColumns() + " " + this.getRows() + "\n" + b.getColumns() + " " + b.getRows());
        }

        float[][] zeroMatrix = new float[this.getRows()][b.getColumns()];
        Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                resultMatrix.getArray()[i][j] = this.getArray()[i][j] * b.getArray()[i][j];
            }
        }
        
        //System.out.println("In hadProd ");
        //checkNaN(resultMatrix);
        return resultMatrix;
    }
    /*
    matrix multiplication
     */
    public Matrix mmul( Matrix b) {
        if (this.getColumns() != b.getRows()) {
            System.out.println("MATRIX MISMATCH ON MULTIPLY");
            System.out.println(this.getRows() + " " + this.getColumns() + "\n" + b.getRows() + " " + b.getColumns());
        }
        double[][] resultMatrix = new double[this.getRows()][b.getColumns()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < resultMatrix.length; i++) {
            for (int j = 0; j < resultMatrix[0].length; j++) {
                double sum = 0.0;
                double c = 0.0;
                for (int k = 0; k < this.getColumns(); k++) {
                    /*if (Double.isNaN(this.getArray()[i][k])) System.out.println("fudegE");
                    if (Double.isNaN(b.getArray()[k][j])) System.out.println("fudegE22222");
                    if (Double.isNaN(this.getArray()[i][k] * b.getArray()[k][j])) System.out.println("Multifuck");
                    if (Double.isNaN(c)) System.out.println("c so bad loel " + c);*/
                    double y = this.getArray()[i][k] * b.getArray()[k][j] - c;
                    //sum = sum + this.getArray()[i][k] * b.getArray()[k][j];
                    /*if (Double.isNaN(y)) {
                        System.out.println("y tho");
                        System.out.println(y);
                        System.out.println(this.getArray()[i][k]);
                        System.out.println(b.getArray()[k][j]);
                        System.exit(0);
                    }*/

                    double t = y + sum;
                    c = ( t - sum) - y;
                    /*if (Double.isNaN(t)) {
                        System.out.println("t tho");
                        System.out.println(t);
                        System.out.println(this.getArray()[i][k]);
                        System.out.println(b.getArray()[k][j]);
                        System.exit(0);
                    }
                    
                    if (Double.isNaN(sum)) {
                        System.out.println("sum tho");
                        System.out.println(sum);
                        System.out.println(this.getArray()[i][k]);
                        System.out.println(b.getArray()[k][j]);
                        System.exit(0);
                    }*/
                    
                    sum = t;
                    
                    /*if (Double.isNaN(sum)) {
                        System.out.println("sum tho at end");
                        System.out.println(sum);
                        System.out.println(this.getArray()[i][k]);
                        System.out.println(b.getArray()[k][j]);
                        System.exit(0);
                    }
                    
                    if (Double.isNaN(c)) {
                        System.out.println("c tho at end " + c);
                        System.out.println("t " + t);
                        System.out.println("sum " + sum);
                        System.out.println("first mat ele " + this.getArray()[i][k]);
                        System.out.println("second mat ele " + b.getArray()[k][j]);
                        Thread.dumpStack();
                        System.exit(0);
                    }
                    
                    
                    //resultMatrix[i][j] += (this.getArray()[i][k] * b.getArray()[k][j]);
                    
                    if (Double.isNaN(sum)) System.out.println("Oh shit son");*/
                }
                resultMatrix[i][j] =  sum;
            }
        }
        //System.out.println("In mmul ");
        //checkNaN(new Matrix(resultMatrix));
        return new Matrix(resultMatrix);
    } 
    /*
    matrix subtraction
    */
    public Matrix msub(Matrix b) {
        //double[][] resultMatrix = new double[this.getRows()][this.getColumns()];
        if (this.getRows() != b.getRows() || this.getColumns() != b.getColumns()) {
            System.err.println("ERROR MATRIX MISMATCH MSUB");
            System.out.println(this.getColumns() + " " + this.getRows() + "\n" + b.getColumns() + " " + b.getRows());
        }
        float[][] zeroMatrix = new float[this.getRows()][b.getColumns()];
        Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getColumns(); j++) {
                resultMatrix.getArray()[i][j] = this.getArray()[i][j] - b.getArray()[i][j];
            }
        }
        //System.out.println("In msub ");
        //checkNaN(resultMatrix);
        return resultMatrix;
    }
    /*
    scalar multiplication
    */
    public Matrix smul(float scalar) {
        float[][] resultMatrix = new float[this.getRows()][this.getColumns()];
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                resultMatrix[i][j] = this.getArray()[i][j] * scalar;
            }
        }
        
        //System.out.println("In smul ");
        //checkNaN(new Matrix(resultMatrix));
        return new Matrix(resultMatrix);
    }
    
    public Matrix log() {
        float[][] resultMatrix = new float[this.getRows()][this.getColumns()];
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                resultMatrix[i][j] = (float) Math.log(this.getArray()[i][j]);
            }
        }
        return new Matrix(resultMatrix);
    }

    public void setVals(float[][] targOutArray) {
        this.matrixValues = targOutArray;
    }
    
    public Matrix pow(float pow) {
        float[][] resultMatrix = new float[this.getRows()][this.getColumns()];
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                resultMatrix[i][j] = (float) Math.pow(this.getArray()[i][j], pow);
                //System.out.print(resultMatrix[i][j] + " ");
                //if (Double.isNaN(resultMatrix[i][j])) {
                    //System.out.println(resultMatrix[i][j]);
                    //System.out.println("NaN VALUE HANDED TO MATRIX.POW");
                //}
            }
        }
        //System.out.println();
        //System.out.println("In pow ");
        //checkNaN(new Matrix(resultMatrix));
        return new Matrix(resultMatrix);
    }
    
    public void printMatrix() {
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                System.out.print(this.matrixValues[i][j] + " ");
                //resultMatrix[i][j] = Math.pow(this.getArray()[i][j], pow);
                //System.out.print(resultMatrix[i][j] + " ");
                //if (Double.isNaN(resultMatrix[i][j])) {
                    //System.out.println(resultMatrix[i][j]);
                    //System.out.println("NaN VALUE HANDED TO MATRIX.POW");
                //}
            }
            System.out.println();
        }
    }
    
    /*
    Sums over the all the entries in a Matrix to find a single number that 
    can be used for debugging. For example, the number returned can be used
    during backprop debugging to look for whether weights are changing from
    epoch to epoch. 
    */
    public float calculateMagnitude() {
        float sum = 0;
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                sum+= Math.abs(this.matrixValues[i][j]);
            }
        }
        return sum;
        
        //System.out.println("Weight magnitude is " + sum);
    }
    
    /*
    Check entire matrix for NaN values
    */
    public static boolean checkNaN(Matrix M) {
        for (int i = 0; i < M.getRows(); i++) {
            for (int j = 0; j < M.getColumns(); j++) {
                if (Double.isNaN(M.matrixValues[i][j])) {
                    System.out.println("Found a NaN value.");
                    //M.printMatrix();
                    Thread.dumpStack();
                    System.exit(0);
                    return true;
                }
            }
        }
        return false;
    }
}