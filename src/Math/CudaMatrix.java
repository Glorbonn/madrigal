package Math;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import jcuda.Pointer;
import jcuda.Sizeof;
import static jcuda.jcublas.JCublas.cublasAlloc;
import static jcuda.jcublas.JCublas2.*;
import jcuda.jcublas.cublasHandle;
import static jcuda.jcublas.cublasOperation.CUBLAS_OP_N;
import jcuda.runtime.JCuda;
import static jcuda.runtime.JCuda.*;
import static jcuda.runtime.cudaMemcpyKind.cudaMemcpyDeviceToHost;
import static jcuda.runtime.cudaMemcpyKind.cudaMemcpyHostToDevice;
/**
 *
 * @author Ross Wendt
 */
public class CudaMatrix {
    
    private final int rows;
    private final int columns;
    private float[][] matrixValues;
    
    /*
    initialize a concatenated matrix from a matrix arraylist
    */
    
    public CudaMatrix() {
        rows = 1;
        columns = 1;
        matrixValues = new float[1][1];
    }
    
    public CudaMatrix(List<Matrix> initialMatrixValues) {
        rows = initialMatrixValues.size();
        columns = initialMatrixValues.get(0).getColumns();
        matrixValues = new float[rows][columns];
        for (int i = 0; i < rows; i++) {
            matrixValues[i] = initialMatrixValues.get(i).getArray()[0];
            //for (int j = 0; j < rows; j++) {
            //    matrixValues[j] = initialMatrixValues.get(i).getArray()[0];
            //}
        }
    }
    
    /*
    initialize a matrix from a double list.
    */
    public CudaMatrix(ArrayList<Float> initialMatrixValues) {
        rows = 1;
        columns = initialMatrixValues.size();
        matrixValues = new float[rows][columns];
        float[] d = new float[columns];
        for (int i = 0; i < columns; i++) {
            d[i] = initialMatrixValues.get(i);
        }
        matrixValues[0] = d;
        //matrixValues[0] = initialMatrixValues.toArray(new double[5]);
    }
    
    /*
    initializes a matrix based on a 2D double array
    */    
    public CudaMatrix(float[][] initialMatrixValues){
        rows = initialMatrixValues.length;
        columns = initialMatrixValues[0].length;
        //matrixValues = new double[rows][columns];
        matrixValues = initialMatrixValues;
        //for(int i = 0; i < matrixValues.length; i++){
        //    System.arraycopy(initialMatrixValues[i], 0, matrixValues[i], 0, matrixValues[0].length);
        //}
    }
    /*
    initializes a Matrix based on 1D double array
    */
    public CudaMatrix(float[] initialMatrixValues){
        rows = 1;
        columns = initialMatrixValues.length;
        matrixValues = new float[rows][columns];
        matrixValues[0] = initialMatrixValues;
        //System.arraycopy(initialMatrixValues, 0, matrixValues[0], 0, matrixValues[0].length);
    }   

    public CudaMatrix(Map.Entry<ArrayList<Double>, ArrayList<Double>> get) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public CudaMatrix(float[][] initialMatrix, float val) {
        rows = initialMatrix.length;
        columns = initialMatrix[0].length;
        matrixValues = new float[rows][columns];
        for (int i = 0; i < initialMatrix.length; i++ ) {
            for (int j = 0; j < initialMatrix[0].length; j++) {
                matrixValues[i][j] = val;
            }
        }
    }

    public int getRows(){
        return rows;
    }   
    public int getColumns(){
        return columns;
    }   
    public float[][] getArray(){
        return matrixValues;
    }   
    public float[][] getMatrixValues(){
        return matrixValues;
    }
    public void setMatrixValues(int index1, int index2, float value) {
        matrixValues[index1][index2] = value;
    }
    /*
    matrix transposition
    */
    public Matrix transpose() {
        //if (this.getColumns() == this.getRows()) {
        //    System.out.println("YES");
        //}
        float[][] resultMatrix = new float[this.getColumns()][this.getRows()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        int rows = this.getRows();
        int columns = this.getColumns();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                resultMatrix[j][i] = this.getArray()[i][j];
            }
        }
        return new Matrix(resultMatrix);
    }
    /*
    matrix addition
    */
    public Matrix madd(Matrix b) {
        //double[][] resultMatrix = new double[this.getRows()][this.getColumns()];
        
        float[][] zeroMatrix = new float[this.getRows()][b.getColumns()];
        Matrix resultMatrix = new Matrix(zeroMatrix);
        
        /*if (this.getRows() != b.getRows() || this.getColumns() != b.getColumns()) {
            System.err.println("ERROR MATRIX MISMATCH MADD");
            System.out.println(this.getColumns() + " " + this.getRows() + "\n" + b.getColumns() + " " + b.getRows());
        }*/
        
        
        /*cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, n, n, 
            pAlpha, d_A, n, d_B, n, pBeta, d_C, n);*/
        
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getColumns(); j++) {
                //this.getArray()[i][j] += b.getArray()[i][j];
                resultMatrix.getArray()[i][j] = this.getArray()[i][j] + b.getArray()[i][j];
            }
        }
        return resultMatrix;
    }
    /*
    adds bias value to all elements of target matrix
    */
    /*public Matrix addBiasMatrices(Matrix bias) {
        double[][] resultMatrix = new double[this.getRows()][this.getColumns()];
        //double[][] zeroMatrix = new double[this.getRows()][this.getColumns()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < bias.getColumns(); i++) {
            for (int j = 0; j < this.getRows(); j++) {
                for (int k = 0; k < this.getColumns(); k++) {
                    resultMatrix[j][k] = this.getArray()[j][k] + bias.getArray()[0][i];
                }
            }
        }
        return new Matrix(resultMatrix);
    }*/
    /*
    randomly initializes matrix with upperbound
    */
    public CudaMatrix init_rand(float upperBound) {
        //Random randomGenerator = new Random();
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                //double lowerBound = 0;
                float randomValue = (float) ThreadLocalRandom.current().nextDouble(-upperBound,upperBound);//lowerBound + (upperBound - lowerBound) * randomGenerator.nextDouble();
                this.getArray()[i][j] = randomValue;
            }
        }
        return this;
    }
    /*
    hadamard product, also called element-wise product
    */
    public Matrix hadProd(Matrix b) {
        //double[][] resultMatrix = new double[this.getRows()][this.getColumns()];
        
        /*if (this.getRows() != b.getRows() || this.getColumns() != b.getColumns()) {
            System.err.println("ERROR MATRIX MISMATCH HADAMARD");
            System.out.println(this.getColumns() + " " + this.getRows() + "\n" + b.getColumns() + " " + b.getRows());
        }*/

        float[][] zeroMatrix = new float[this.getRows()][b.getColumns()];
        Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                resultMatrix.getArray()[i][j] = this.getArray()[i][j] * b.getArray()[i][j];
            }
        }
        return resultMatrix;
    }
    /*
    matrix multiplication
     */
    public Matrix sgemm(Matrix b) {
        
        JCuda.setExceptionsEnabled(true);
        
        if (this.getColumns() != b.getRows()) {
            System.out.println("MATRIX MISMATCH ON MULTIPLY");
            System.out.println(this.getColumns() + " " + this.getRows() + "\n" + b.getColumns() + " " + b.getRows());
        }
        //float[][] resultMatrix = new float[this.getRows()][b.getColumns()];
        
        float[][] zeroMatrix = new float[this.getRows()][b.getColumns()];
        Matrix resultMatrix = new Matrix(zeroMatrix);
        
        //float[] a_unwrapped = flatten(this.getMatrixValues());
        //float[] b_unwrapped = flatten(b.getMatrixValues());
        //float[] c_unwrapped = flatten(resultMatrix.getArray());
        
        //float[] return_unwrapped = flatten(resultMatrix.getArray());
       
        
        int size_a = this.getRows()*this.getColumns();
        int size_b = b.getRows()*b.getColumns();
        int size_c = this.getRows()*b.getColumns();
        
        cublasHandle handle = new cublasHandle();
        cublasCreate(handle);
        
        Pointer d_A = new Pointer();
        Pointer d_B = new Pointer();
        Pointer d_C = new Pointer();
        Pointer pAlpha = Pointer.to(new float[]{1.0f});
        Pointer pBeta = Pointer.to(new float[]{0.0f});
        
        cublasAlloc(size_a, Sizeof.FLOAT, d_A);
        cublasAlloc(size_b, Sizeof.FLOAT, d_B);
        cublasAlloc(size_c, Sizeof.FLOAT, d_C);
        
        copyToDevice(d_A, this.getArray());
        copyToDevice(d_B, b.getArray());
        copyToDevice(d_C, resultMatrix.getArray());
        
        //cudaMalloc(d_A, size_a * Sizeof.FLOAT);
        //cudaMalloc(d_B, size_b * Sizeof.FLOAT);     
        //cudaMalloc(d_C, size_c * Sizeof.FLOAT);

        //cublasSetMatrix(this.getRows(), this.getColumns(), Sizeof.FLOAT, Pointer.to(this.getArray()[0]), this.getRows(), d_A, this.getColumns());
        //cublasSetMatrix(b.getRows(), b.getColumns(), Sizeof.FLOAT, Pointer.to(b.getArray()[0]), b.getRows(), d_B, b.getColumns());
        //cublasSetMatrix(resultMatrix.getRows(), resultMatrix.getColumns(), Sizeof.FLOAT, Pointer.to(resultMatrix.getArray()[0]), resultMatrix.getRows(), d_C, resultMatrix.getColumns());
        
        //System.out.println(this.getRows() + " " + this.getColumns() + " " + b.getRows() + " " + b.getColumns() + " " + resultMatrix.getRows() + " " + resultMatrix.getColumns());
        
        
        cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, this.getRows(), b.getColumns(), this.getColumns(), 
                pAlpha, d_A, this.getRows(), d_B, b.getRows(), pBeta, d_C, resultMatrix.getRows());
        
        copyFromDevice(resultMatrix.getArray(), d_C);
        
        /*cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, n, n, 
            pAlpha, d_A, n, d_B, n, pBeta, d_C, n);*/
        
        //cublasGetMatrix(resultMatrix.getRows(), resultMatrix.getColumns(), Sizeof.FLOAT, d_C, resultMatrix.getRows(), Pointer.to(resultMatrix.getArray()[0]), resultMatrix.getRows());
        //cublasGetVector(nn, Sizeof.FLOAT, d_C, 1, Pointer.to(C), 1);
        
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        /*for (int i = 0; i < resultMatrix.getRows(); i++) {
            for (int j = 0; j < resultMatrix.getColumns(); j++) {
                for (int k = 0; k < this.getColumns(); k++) {
                    resultMatrix.getArray()[i][j] += (this.getArray()[i][k] * b.getArray()[k][j]);
                }
            }
        }*/
        //resultMatrix = wrap(return_unwrapped, resultMatrix.getRows(), resultMatrix.getColumns());
        
        // Clean up
        cudaFree(d_A);
        cudaFree(d_B);
        cudaFree(d_C);
        cublasDestroy(handle);
        
        return resultMatrix;
    } 
    
    public Matrix mmul( Matrix b) {
        if (this.getColumns() != b.getRows()) {
            System.out.println("MATRIX MISMATCH ON MULTIPLY");
            System.out.println(this.getColumns() + " " + this.getRows() + "\n" + b.getColumns() + " " + b.getRows());
        }
        float[][] resultMatrix = new float[this.getRows()][b.getColumns()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < resultMatrix.length; i++) {
            for (int j = 0; j < resultMatrix[0].length; j++) {
                for (int k = 0; k < this.getColumns(); k++) {
                    resultMatrix[i][j] += (this.getArray()[i][k] * b.getArray()[k][j]);
                }
            }
        }
        return new Matrix(resultMatrix);
    } 
    
    /*public Matrix mmul( Matrix b) {
        /*if (this.getColumns() != b.getRows()) {
            System.out.println("MATRIX MISMATCH ON MULTIPLY");
            System.out.println(this.getColumns() + " " + this.getRows() + "\n" + b.getColumns() + " " + b.getRows());
        }*/
    /*    float[][] resultMatrix = new float[this.getRows()][b.getColumns()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
       
        System.out.println(this.getRows() + " " + this.getColumns());
        System.out.println(b.getRows() + " " + b.getColumns());
        System.out.println(resultMatrix.length + " " + resultMatrix[0].length);
        
        for (int i = 0; i < resultMatrix.length; i++) {
            for (int j = 0; j < resultMatrix[0].length; j++) {
                for (int k = 0; k < this.getRows(); k++) {
                    resultMatrix[i][j] += (this.getArray()[i][k] * b.getArray()[k][j]);
                }
            }
        }
        return new Matrix(resultMatrix);
    } */
    /*
    matrix subtraction
    */
    public Matrix msub(Matrix b) {
        /*double[][] resultMatrix = new double[this.getRows()][this.getColumns()];
        if (this.getRows() != b.getRows() || this.getColumns() != b.getColumns()) {
            System.err.println("ERROR MATRIX MISMATCH MSUB");
            System.out.println(this.getColumns() + " " + this.getRows() + "\n" + b.getColumns() + " " + b.getRows());
        }*/
        float[][] zeroMatrix = new float[this.getRows()][b.getColumns()];
        Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getColumns(); j++) {
                resultMatrix.getArray()[i][j] = this.getArray()[i][j] - b.getArray()[i][j];
            }
        }
        return resultMatrix;
    }
    /*
    scalar multiplication
    */
    public Matrix smul(float scalar) {
        float[][] resultMatrix = new float[this.getRows()][this.getColumns()];
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                resultMatrix[i][j] = this.getArray()[i][j] * scalar;
            }
        }
        return new Matrix(resultMatrix);
    }

    public void setVals(float[][] targOutArray) {
        this.matrixValues = targOutArray;
    }
    
    public Matrix pow(float pow) {
        float[][] resultMatrix = new float[this.getRows()][this.getColumns()];
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                resultMatrix[i][j] = (float) Math.pow(this.getArray()[i][j], pow);
                //System.out.print(resultMatrix[i][j] + " ");
                //if (Double.isNaN(resultMatrix[i][j])) {
                    //System.out.println(resultMatrix[i][j]);
                    //System.out.println("NaN VALUE HANDED TO MATRIX.POW");
                //}
            }
        }
        //System.out.println();
        return new Matrix(resultMatrix);
    }
    
    public void printMatrix() {
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                System.out.print(this.matrixValues[i][j] + " ");
                //resultMatrix[i][j] = Math.pow(this.getArray()[i][j], pow);
                //System.out.print(resultMatrix[i][j] + " ");
                //if (Double.isNaN(resultMatrix[i][j])) {
                    //System.out.println(resultMatrix[i][j]);
                    //System.out.println("NaN VALUE HANDED TO MATRIX.POW");
                //}
            }
        }
        System.out.println();
    }
    
    private Matrix wrap(float[] flattened, int rows, int columns) {
        Matrix ret = new Matrix(new float[rows][columns]);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                ret.getArray()[i][j] = flattened[i+j];
            }
        }
        
        return ret;
        
    }

    private float[] flatten(float[][] matrixValues) {
        float[] ret = new float[matrixValues.length * matrixValues[0].length];
        for (int i = 0; i < matrixValues.length; i++) {
            for (int j = 0; j < matrixValues[0].length; j++) {
                ret[i + j] = matrixValues[i][j];
            }
        }
        return ret;
    }
    
       /**
     * Copies the data from the given array to the given device pointer.
     *
     * @param device The pointer to copy to
     * @param array The array to copy from
     */
    private static void copyToDevice(Pointer device, float array[][])
    {
        int rowSizeBytes = Sizeof.FLOAT * array[0].length;
        for (int i = 0; i < array.length; i++)
        {
            Pointer deviceRow = device.withByteOffset(rowSizeBytes * i);
            cudaMemcpy(deviceRow, Pointer.to(array[i]),
                rowSizeBytes, cudaMemcpyHostToDevice);
        }
    }
 
    /**
     * Copies the data from the given device pointer to the given array.
     *
     * @param array The array to copy to
     * @param device The pointer to copy from
     */
    private static void copyFromDevice(float array[][], Pointer device)
    {
        int rowSizeBytes = Sizeof.FLOAT * array[0].length;
        for (int i = 0; i < array.length; i++)
        {
            Pointer deviceRow = device.withByteOffset(rowSizeBytes * i);
            cudaMemcpy(Pointer.to(array[i]), deviceRow,
                rowSizeBytes, cudaMemcpyDeviceToHost);
        }
    }
}