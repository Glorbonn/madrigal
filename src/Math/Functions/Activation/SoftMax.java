/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Math.Functions.Activation;

/**
 *
 * @author rossw
 */
public class SoftMax extends AbstractFunction {

    @Override
    float[] function(float[] inputs, float[] groundTruth) {
        float[] classProbability = new float[inputs.length];
        float[] shiftedInput = new float[inputs.length];
        float max = -Float.MAX_VALUE;
        // normalize by shifting the values such that 0 is the highest
        
        // Find max value
        for (int i = 0; i < inputs.length; i++ ) {
            if (inputs[i] >= max) {
                max = inputs[i];
            }
        }
        
        // Subtract max value
        
        for (int i = 0; i < shiftedInput.length; i++) {
            shiftedInput[i] = inputs[i] - max;
        }
        
        float sum = 0;
        for (int i = 0; i < shiftedInput.length; i++ ) {
           sum += Math.exp(shiftedInput[i]);
        }
        
        for (int i = 0; i < shiftedInput.length; i++) {

            classProbability[i] = (float) Math.exp(shiftedInput[i]) / sum;
        }
        
        return classProbability;
    }

    @Override
    float[] functionDerivative(float[] in, float[] groundTruth) {
        float[] softmax = function(in);
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
