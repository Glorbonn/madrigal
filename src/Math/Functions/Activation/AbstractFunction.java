package Math.Functions.Activation;
import Math.Matrix;
/*
*
*
*
*/
public abstract class AbstractFunction {
    //float y;
    
    abstract float[] function(float[] in);
    abstract float[] functionDerivative(float[] in);   
    public Matrix applyDerivative(Matrix a) {
        Matrix M = new Matrix(new float[a.getRows()][a.getColumns()]);
        for (int i = 0; i < a.getRows(); i++) {
            M.getArray()[i] = functionDerivative(a.getArray()[i]);
            //for (int j = 0; j < a.getColumns(); j++) {
            //    M.getArray()[i][j] = functionDerivative(a.getArray()[i][j]);
            //}
        }
        return M;
    }
    public Matrix apply(Matrix a) {
        Matrix M = new Matrix(new float[a.getRows()][a.getColumns()]);
        for (int i = 0; i < a.getRows(); i++) {
            M.getArray()[i] = function(a.getArray()[i]);
            //for (int j = 0; j < a.getColumns(); j++) {
            //    M.getArray()[i][j] = function(a.getArray()[i][j]);
            //}
        }
        return M;
    }    
}
