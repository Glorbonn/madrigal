package Math.Functions.Activation;

public class Sigmoid extends AbstractFunction {
    @Override
    /*
    Fast exp cite: "A Fast, Compact Approximation of the Exponential Function"
    
    Provided by martin ankerl @ martin.ankerl.com
    */
    public float[] function(float[] x) {
        //final long tmp = (long) (1512775 * x + 1072632447);
        //return Double.longBitsToDouble(tmp << 32);
        //return 1 / (1 + (float) Math.exp(-x));
        //return 1 / (1+ (1+x/1.0 + (x*x)/2 + (x*x*x)/6 + (x*x*x*x/12)));
        //return (x / 1 + Math.abs(x));
        float[] y = x.clone();
        
        
        for (int i = 0; i < x.length; i++) {
            if (y[i] >= 0) {
                float z = (float) Math.exp(-y[i]);
                y[i] = 1 / (1+ z);
            } else {
                float z = (float) Math.exp(y[i]);
                y[i] = z / (1 + z);
            }
        }
        
        return y;
    }
    @Override
    public float[] functionDerivative(float[] x) {
        float[] y = function(x.clone());
        for (int i = 0; i < x.length; i++) {
            y[i] *= 1 - y[i];
        }
        //y *= 1 - y;
        return y;
    }
}
