package Math.Functions.Activation;

public class HyperbolicTangent extends AbstractFunction {
    /*
    Fast tanh from http://www.musicdsp.org/showone.php?id=238
    
    pade approximation
    */
    @Override
    public float[] function(float[] x) {
        float[] y = new float[x.length];
        for (int i = 0; i < x.length; i++) {
            if( x[i] < -3 )
                y[i] = -1;
            else if( x[i] > 3 )
                y[i] = 1;
            else
                y[i] = x[i] * ( 27 + x[i] * x[i]) / ( 27 + 9 * x[i] * x[i] );
        }
        return y;  
    }
    @Override
    public float[] functionDerivative(float[] x) {
        float y[] = function(x);
        
        for (int i = 0; i < x.length; i++ ) {
            y[i] *= y[i];
            y[i] = 1 - y[i];
        }
        
        //y*=y;
        //y = 1 - y;
        return y;    
    }
    
}
