package Math.Functions.Activation;

/**
 *
 * @author Ross Wendt
 */
public class RectifiedLinear extends AbstractFunction {

    @Override
    float function(float in) {
        return Math.max(0, in);
    }

    @Override
    float functionDerivative(float in) {
        if ( in < 0 ) {
            return 0;
        } else {
            return 1;
        }
    }
    
}
