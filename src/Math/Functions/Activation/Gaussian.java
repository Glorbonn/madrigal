package Math.Functions.Activation;

/**
 *
 * @author Ross Wendt/ Lukas Keller
 */
public class Gaussian extends AbstractFunction {
    public float r;
    public float c;

    public Gaussian(float radius, float center) {
        r = radius;
        c = center;
    }
    @Override
    public float function(float x) {
        float d = (x - c) * (x - c); 
        float y = (float) Math.exp(-((d) / (r * r)));  

        return y;    
}
    @Override
    public float functionDerivative(float x) {

        float d = (x - c) * (x - c); 
        float temp = (float) Math.exp(- ((d) / (r * r)));
        float y = ((2 * c * temp) / (r * r)) - ((2 * x * temp) / (r * r));  

        return y;    
    }    
}
