package Math.Functions.Simple;

import DataPack.GenerateValues;
import java.util.ArrayList;

/**
 *
 * @author Ross Wendt
 */
public abstract class SimpleFunction extends GenerateValues {
    
    @Override
    public abstract float computeFunction(ArrayList<Float> x);
}
