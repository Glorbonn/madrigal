package Math.Functions.Simple;

import java.util.ArrayList;

/**
 *
 * @author Ross Wendt
 */
public class Rosenbrock extends SimpleFunction {
    
    
 
    
    @Override
    public float computeFunction(ArrayList<Float> x) {
    float y = 0;
        for (int i = 0; i < x.size() - 1; i++) {
            y += 100 * (float) Math.pow(x.get(i+1) - (float) Math.pow(x.get(i),2),2) + (float) Math.pow(1 - x.get(i), 2);
        }
        return y;
    }
    


}
