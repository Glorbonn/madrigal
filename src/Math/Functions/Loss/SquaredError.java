/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Math.Functions.Loss;

import Math.Matrix;
import static Math.Matrix.checkNaN;

/**
 *
 * @author rossw
 */
public class SquaredError extends AbstractLoss {

    @Override
    public Matrix computeLoss(Matrix groundTruth, Matrix prediction) {
        Matrix errorMatrix = groundTruth.msub(prediction);//targetOutput.msub(Net.output.transpose());
        errorMatrix = errorMatrix.pow(2.0f);
        /*System.out.println("PREDICTED VALUE");
        prediction.printMatrix();
        System.out.println("GROUND TRUTH");
        groundTruth.printMatrix();*/
        
        checkNaN(errorMatrix);
        return errorMatrix;
    }
    
}
