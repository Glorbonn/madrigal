/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Math.Functions.Loss;

import Math.Matrix;
import static Math.Matrix.checkNaN;

/**
 *
 * @author rossw
 */
public class AbsoluteError extends AbstractLoss {

    @Override
    public Matrix computeLoss(Matrix actual, Matrix predicted) {
        Matrix errorMatrix = predicted.msub(actual);//targetOutput.msub(Net.output.transpose());
        checkNaN(errorMatrix);
        return errorMatrix;    
    }
    
}
