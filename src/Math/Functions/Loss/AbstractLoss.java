/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Math.Functions.Loss;
import Math.Matrix;

/**
 *
 * @author rossw
 */
public abstract class AbstractLoss {
    public abstract Matrix computeLoss(Matrix actual, Matrix predicted);
}
