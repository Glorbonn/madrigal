/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Math.Functions.Loss;

import DriverPack.MiniLib;
import Math.Matrix;
import static Math.Matrix.checkNaN;

/**
 *
 * @author rossw
 * 
 * def CrossEntropy(yHat, y):
 *   if y == 1:
      return -log(yHat)
    else:
      return -log(1 - yHat)
 */
public class CrossEntropy extends AbstractLoss {
    @Override
    public Matrix computeLoss(Matrix actual, Matrix predicted) {
        //System.out.println("Example:");
        float[][] actualArray = actual.getArray();
        float[][] predictedArray = predicted.getArray();
        float[][] errorArray = new float[actualArray.length][actualArray[0].length]; //will be overwritten
        //double[][] softmaxPredictedArray = new double[actualArray.length][actualArray[0].length];
        
        //for (int i = 0; i <actualArray.length; i++) {
        //    softmaxPredictedArray[i] = MiniLib.softMax(predictedArray[i]);
        //}
        
        for (int i = 0; i < actualArray.length; i++) {
            for (int j = 0; j < actualArray[0].length; j++) {
                //errorArray[i][j] =  (float) ((float) - actualArray[i][j] * Math.log(predictedArray[i][j]));
                /*if (errorArray[i][j] > 0.05) {
                    float loss = errorArray[i][j];
                    for (int k = 0; k < actualArray.length; k++) {
                        for (int l = 0; l < actualArray[0].length; l++) {
                            errorArray[k][l] = loss;
                        }
                    }
                    break;
                }*/
                if (Math.abs(actualArray[i][j] - 1) < 1e-6 ) {
                    /*if (predictedArray[i][j] == 0 || predictedArray[i][j] == -0) {
                        System.out.println("0 LOG");
                        System.exit(0);
                    }*/
                    //float fuejheub = 0;
                    errorArray[i][j] = (Float.compare(1-predictedArray[i][j], 0) == 0) ? -100.0f: (float) -Math.log(1-predictedArray[i][j]);
                    //if (Float.compare(1-predictedArray[i][j], 0) == 0) System.out.println("0 log");
                    //errorArray[i][j] = (float) -Math.log(1 - predictedArray[i][j]);

                } else {
                    /*if (predictedArray[i][j] == -0 || predictedArray[i][j] == 0) {
                        System.out.println("0 LOG");
                        System.exit(0);
                    }*/
                    errorArray[i][j] = (Float.compare(predictedArray[i][j], 0) == 0) ? -100.0f: (float) -Math.log(predictedArray[i][j]);

                    //if (Float.compare(predictedArray[i][j], 0) == 0) System.out.println("0 log");

                    //errorArray[i][j] = (float) -Math.log(predictedArray[i][j]);

                }
                //System.out.format("\tError: " + errorArray[i][j] + "\t\t Actual:" + actualArray[i][j] + "\t\tPredicted: " + predictedArray[i][j] + "\n"); 
            }
        }
        
        Matrix computedLoss = new Matrix(errorArray);
        
        /*System.out.println("Error in loss function is:" );
        computedLoss.printMatrix();*/
        checkNaN(computedLoss);
        
        
        return new Matrix(errorArray);
        
    }
}
