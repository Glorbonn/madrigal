package NeuralNet;

import Math.Matrix;
import java.util.ArrayList;
import javafx.util.Pair;

/**
 *
 * @author Ross Wendt
 */
public abstract class NetworkInterface {
    public int epochLimit;
    
    abstract Matrix[] initializeWeights(int[] hiddenLayers);
    public abstract float[][] dataPropagation(ArrayList<Pair<Matrix, Matrix>> example);
    
    public void setEpochLimit(int in) {
        epochLimit = in;
    
    }
        
    public int getEpochLimit() {
        return epochLimit;
    }
}
