package NeuralNet;
import DriverPack.Controller;
import java.util.Random;
import DriverPack.MiniLib;
import Math.Functions.Activation.AbstractFunction;
import Math.Functions.Loss.*;
import Math.Matrix;
import static Math.Matrix.checkNaN;
import NeuralNet.TrainingMethod.TrainingMethodInterface;
import java.util.ArrayList;
import java.util.UUID;
import javafx.util.Pair;
public class FeedforwardNetwork extends NetworkInterface {
    public String netCode;// = Integer.toString(this.getClass().hashCode()*System.currentTimeMillis,16);
    public static Matrix errorMatrix;
    private final float initWeight;
    private float eta;
    public final double exponent;
    public float momentumParameter;
    
    public final double epochLimit;
    public final int[] hiddenLayers;
    
    private final double upperBoundInitializationBias;


    private boolean isHiddenLayerCountZero;
    public Matrix input;
    public Matrix output;
    public Matrix targetOutput;
    public  Matrix[] weights;
    public Matrix[] bias;
    public Matrix[] layerOutput;
    public Matrix[] layerOutputWithActivationFunctionApplied;
    public Matrix[] functionApplied;
    public Matrix[] derivativeApplied;
    public Matrix[] gradient;
    public Matrix[] lastWeights;
    public Matrix[] lastBiasUpdates;
    public AbstractFunction[] activationFunctionInterface;
    public TrainingMethodInterface trainingMethodInterface;
    public AbstractLoss lossFunction;

    // initialize the Matrix
    public FeedforwardNetwork(Matrix input, 
            Matrix targetOutput, 
            int[] hiddenLayers, 
            float upperBoundInitializationWeight, 
            float upperBoundInitializationBias, 
            float eta, double exponent, int epochLimit, 
            float momentumParameter, 
            AbstractFunction[] activationFunctionInterface, 
            TrainingMethodInterface trainingMethodInterface,
            AbstractLoss lossFunction) {
        netCode = UUID.randomUUID().toString();
        this.eta = eta;
        this.hiddenLayers = hiddenLayers;
        this.exponent = exponent;
        this.epochLimit = epochLimit;
        this.initWeight = upperBoundInitializationWeight;
        this.momentumParameter = momentumParameter;
        this.activationFunctionInterface = activationFunctionInterface;
        this.trainingMethodInterface = trainingMethodInterface; 
        this.upperBoundInitializationBias = upperBoundInitializationBias;

        
        this.input = input;
        this.output = targetOutput;
        this.targetOutput = targetOutput;
        this.lossFunction = lossFunction;
        isHiddenLayerCountZero = (hiddenLayers.length == 0);
        weights = initializeWeights(hiddenLayers);
        bias = initializeBias(hiddenLayers, upperBoundInitializationBias);
        lastBiasUpdates = bias;
        gradient = initializeGradient(new Matrix[hiddenLayers.length + 1]); //not used
        lastWeights = initializeLastWeights(new Matrix[weights.length]);
        layerOutput = new Matrix[hiddenLayers.length];
        layerOutputWithActivationFunctionApplied = new Matrix[hiddenLayers.length];
        functionApplied = new Matrix[hiddenLayers.length];
        derivativeApplied = new Matrix[hiddenLayers.length];
    }
    
    private Matrix[] initializeLastWeights(Matrix[] in) {
        /*Matrix[] initializeLastWeights = in;
        for (int i = 0; i < weights.length; i++) {
            initializeLastWeights[i] = new Matrix(new float[weights[i].getRows()][weights[i].getColumns()]);
        }*/
        
        return weights.clone();
    }
    
    private Matrix[] initializeGradient(Matrix[] in) {
        Matrix[] initializeGradient = in;
        initializeGradient[initializeGradient.length - 1] = new Matrix(new float[targetOutput.getArray()[0].length][targetOutput.getArray().length]); //this is meant to be transposed
        for (int i = initializeGradient.length - 2; i > -1; i--) {
            initializeGradient[i] = weights[i+1].mmul(initializeGradient[i+1]); 
        }
        
        return initializeGradient;
    }
    
    final Matrix[] initializeBias(int[] hiddenLayers, float upperBoundInitializationBias) {
        float randValue = new Random().nextFloat()*upperBoundInitializationBias;
        int layersLength = hiddenLayers.length;
        Matrix[] initWeights = new Matrix[layersLength + 1];
        initWeights[0] = new Matrix(new float[this.input.getColumns()][hiddenLayers[0]], randValue);       
        for (int i = 1; i < hiddenLayers.length; i++) {
            initWeights[i] = new Matrix(new float[hiddenLayers[i - 1]][hiddenLayers[i]], randValue);
        }
        initWeights[initWeights.length - 1] = new Matrix(new float[hiddenLayers[layersLength-1]][targetOutput.getArray()[0].length], randValue);
        /*for (int i = 0; i < initWeights.length; i++) {
            //bound for initialization according to Glorot and Bengio (2010)
            //initWeights[i] = initWeights[i].init_rand(4.0f* (float) Math.sqrt(6.0f/ (initWeights[i].getColumns() + initWeights[i].getRows())));
            initWeights[i] = initWeights[i].init_rand(1.0f);
        }     */         
        return initWeights;
    }
    
    // randomly initialize the node and bias node weights
    @Override
    final Matrix[] initializeWeights(int[] hiddenLayers) {
        int layersLength = hiddenLayers.length;
        Matrix[] initWeights = new Matrix[layersLength + 1];
        initWeights[0] = new Matrix(new float[this.input.getColumns()][hiddenLayers[0]]);       
        for (int i = 1; i < hiddenLayers.length; i++) {
            initWeights[i] = new Matrix(new float[hiddenLayers[i - 1]][hiddenLayers[i]]);
        }
        initWeights[initWeights.length - 1] = new Matrix(new float[hiddenLayers[layersLength-1]][targetOutput.getArray()[0].length]);
        for (int i = 0; i < initWeights.length; i++) {
            //bound for initialization according to Glorot and Bengio (2010)
            initWeights[i] = initWeights[i].init_rand(4.0f* (float) Math.sqrt(6.0f/ (initWeights[i].getColumns() + initWeights[i].getRows())));
            //initWeights[i] = initWeights[i].init_rand(1.0f);
        }              
        return initWeights;
    }  

    @Override
    public float[][] dataPropagation(ArrayList<Pair<Matrix,Matrix>> dataBatch) {
        // Applying weights at input layer
        ArrayList<Matrix> data = new ArrayList<>(MiniLib.getKeys(dataBatch));
        Matrix TrainData = new Matrix(data);
        layerOutput[0] = (TrainData.mmul(weights[0]));      
        //checkNaN(activations[0]);
        // Propagation for each hidden layer to the next, except for the output layer
        for (int i = 0; i < hiddenLayers.length-1; i++) {
            int j = i+1;
            //weights from hidden layer 0 to hidden layer 1 are in weights[1]
            //meaning weights from hidden layer i to hidden layer j are in weights[j]
            //layer output is Z, activations is S, Z is functionApplied
            layerOutputWithActivationFunctionApplied[i] = activationFunctionInterface[0].apply(layerOutput[i]); 
            layerOutput[j] = layerOutputWithActivationFunctionApplied[i].mmul(weights[j]);
            if (i == hiddenLayers.length - 2) {
                functionApplied[i] = (activationFunctionInterface[0].applyDerivative(layerOutput[i])).transpose();//.transpose();   
                functionApplied[j] = (activationFunctionInterface[1].applyDerivative(layerOutput[j])).transpose();//.transpose();//..transpose();  
                layerOutputWithActivationFunctionApplied[j] = activationFunctionInterface[1].apply(layerOutput[j]);
            } else {
                functionApplied[i] = (activationFunctionInterface[0].applyDerivative(layerOutput[i])).transpose();//.transpose();//.transpose();   
            }
        }     
        
        // Propagation for final hidden layer to output
        //layerOutput[layerOutput.length - 1] = layerOutputWithActivationFunctionApplied[layerOutput.length - 2].mmul(weights[layerOutput.length - 1]);
        //layerOutputWithActivationFunctionApplied[layerOutput.length - 1] = activationFunctionInterface[0].apply(layerOutput[layerOutput.length - 1]);
        //Matrix M = activationFunctionInterface[1].applyDerivative(layerOutput[layerOutput.length - 1]);
       // functionApplied[functionApplied.length - 1] = M.transpose();  
        
        output = activationFunctionInterface[1].apply(layerOutput[layerOutput.length - 1].mmul(weights[weights.length - 1]));
        
        targetOutput = new Matrix(MiniLib.getValues(dataBatch));//(Matrix) exampleIn.getValue();
        //errorMatrix = targetOutput.msub(output);//targetOutput.msub(Net.output.transpose());
        //errorMatrix = errorMatrix.pow(2.0f);
        Matrix computedLoss = lossFunction.computeLoss(targetOutput, output);
        errorMatrix = computedLoss;
        Matrix.checkNaN(errorMatrix);
        //float[][] computedLossArray = computedLoss.getArray();
        
        //float error = MiniLib.calcError(errorMatrix);  
        //System.out.println("ERROR INSIDE FF IS " + error);
        return output.getArray();
    } 
    public void setInputMatrix(float[] inputMatrix){
        input = new Matrix(inputMatrix);
    }  
    public Matrix getInputMatrix() {
        return input;
    }
    public Matrix getTargetOutputMatrix() {
        return targetOutput;
    }       
    public TrainingMethodInterface getTrainingMethodInterface() {
        return trainingMethodInterface;
    }   
    public Matrix getOutputMatrix() {
        return output;
    }   
    public float getEta() {
        return eta;
    }
    public Matrix[] getWeights() {
        return this.weights;
    }
}