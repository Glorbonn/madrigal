/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NeuralNet.TrainingMethod.Evolutionary;

import DriverPack.NeuralNetDriver;
import Math.Matrix;
import NeuralNet.FeedforwardNetwork;
import NeuralNet.TrainingMethod.TrainingMethodInterface;
import java.util.ArrayList;
import javafx.util.Pair;

/**
 *
 * @author rossw
 */
public interface GeneticInterface extends TrainingMethodInterface {
    public ArrayList<Matrix[]> initializePopulation(float randomBound, FeedforwardNetwork net, int populationSize);
    public void replacement(Matrix[] sample, Matrix[] mutant, Matrix[] crossover, ArrayList<Pair<Matrix, Matrix>> datum, NeuralNetDriver netDriver);
    public ArrayList<Matrix[]> randomSelect(int numToSelect);
    public Matrix[] selectBest(Matrix[] in, NeuralNetDriver driver);
}
