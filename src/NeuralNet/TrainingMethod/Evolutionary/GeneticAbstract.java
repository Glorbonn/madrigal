package NeuralNet.TrainingMethod.Evolutionary;

import DriverPack.Controller;
import DriverPack.Driver;
import Math.Matrix;
import DriverPack.NeuralNetDriver;
import NeuralNet.FeedforwardNetwork;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import javafx.util.Pair;

/**
 *
 * @author Ross Wendt
 */
public abstract class GeneticAbstract implements GeneticInterface {      
    ArrayList<Matrix[]> Population = new ArrayList<>(); 
    /*
    The purpose of this indexer is to let the program select the next individual 
    from P every epoch. This is so the program can run based on epochs only, so 
    we can compare epochs between backprop and the GA, without running the GA
    |P| times for each epoch. I think this allows more direct comparisons be-
    tween the GAs and backprop, based purely on epoch size
 
    Side effect: if epoch limit < |P|, then we do not run through the
    entire population. Tests will be run with large enough epoch sizes, and
    low enough Population sizes, to avoid this.
    */

    
    /*
    Create a population of size specified, made of random weights.
    */
    @Override
    public ArrayList<Matrix[]> initializePopulation(float randomBound, FeedforwardNetwork net, int populationSize) {
        ArrayList<Matrix[]> Pop = new ArrayList<>();
        Matrix[] W = net.getWeights();
        int length = W.length;
        for (int i = 0; i < populationSize; i++) {                       
            int j = 0;
            Matrix[] individual = new Matrix[length];
            for (Matrix M : W) {
                Matrix L = new Matrix(W[j].getArray());
                L = L.init_rand(randomBound);
                individual[j] = L;
                j++;
                if (j > length ) {
                    break;
                }
            }
            Pop.add(individual);
        }
        return Pop;
    }
    
        
    /*
    Elite selection from the current P(i), mutant, or crossover. Replacement
    of P(i) with most fit individual. Fit individual in from this function is then
    used in neural net. 
    */
    @Override
    public void replacement(Matrix[] sample, Matrix[] mutant, Matrix[] crossover, ArrayList<Pair<Matrix, Matrix>> datum, NeuralNetDriver netDriver) {
        //Net.W = sample;
        double sampleErr = getRMSE(sample, datum, netDriver);
           
        //Net.W = mutant;
        double mutantErr = getRMSE(mutant, datum, netDriver);
        
        //Net.W = crossover;
        double crossoverErr = getRMSE(crossover, datum, netDriver);
        
        if (Math.abs(mutantErr - crossoverErr) < .01) {
            mutantErr -= 0.1;
        }
        
        if (mutantErr < crossoverErr && mutantErr < sampleErr) {
            Population.remove(sample);
            Population.add(mutant);
        } else if (crossoverErr < mutantErr && crossoverErr < sampleErr) {
            Population.remove(sample);
            Population.add(crossover);
        }  
        //System.out.println(Population.size());
    }
    
       
    /*
    Randomly select n individuals (numToSelect) that are unique from P.
    */
    @Override
    public ArrayList<Matrix[]> randomSelect(int numToSelect) {
        Set<Matrix[]> numUnique = new HashSet<>();
        ArrayList<Matrix[]> list = new ArrayList<>();
        
        while ( numUnique.size() < numToSelect) {
            Random rand = new Random();
            int index = rand.nextInt(Population.size());
            Matrix[] selection = Population.get(index);
            numUnique.add(selection);   
        }
        list.addAll(numUnique);
        return list;
    }
    
    @Override
    public Matrix[] selectBest(Matrix[] train, ArrayList<Pair<Matrix, Matrix>> datum, NeuralNetDriver netDriver) {
        //ArrayList<Matrix> a = new ArrayList<>();
        //a.add(example);
        Matrix[] smallest = Population.get(0);
        //Net.W = smallest;
        double smallestErr = getRMSE(train, datum, netDriver);
        //double smallestErr = netDriver.getError(a, index);
        for (int i = 1; i < Population.size(); i++) {
            //Net.W = Population.get(i);
            double error = getRMSE(train, datum, netDriver);
            //double error = netDriver.getError(a, index);
            if ( error < smallestErr ) {
                //Net.W = Population.get(i);
                smallestErr = error;
                smallest = Population.get(i);
            }
            
        }
        return smallest;
    }
    /*
    Need to feedforward in this function with candidate individual,
    calculate the error at the output, and return the error.
    */
    private double getRMSE(Matrix[] individual, ArrayList<Pair<Matrix, Matrix>> datum, NeuralNetDriver netDriver) {
        //MatrixNeuralNet M = Driver.nNet;
        double error = 0;
        return error;
    }

}
