package NeuralNet.TrainingMethod.Evolutionary;

import DriverPack.Driver;
import DriverPack.NeuralNetDriver;
import Math.Matrix;
import NeuralNet.FeedforwardNetwork;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import javafx.util.Pair;

/**
 *
 * @author Ross Wendt
 */
public class GeneticAlgorithm extends GeneticAbstract {
    int lengthWeights = Net.hiddenLayers.length;
    int lengthMatrix = Driver.dataSetSize;   
    boolean init = true;
    
    @Override
    public Matrix applyMethod(ArrayList<Pair<Matrix, Matrix>> in, FeedforwardNetwork net, NeuralNetDriver netDriver) {
        if (init) {
            Population = initializePopulation(1.0f, net, net.getPopulationSize());
            init = false;
            while (Population.size() < net.getPopulationMax()){
                //printInfo();
                ArrayList<Matrix[]> select = randomSelect(2);     
                Matrix[] parent1 = select.get(0);
                Matrix[] parent2 = select.get(1);
                ArrayList<Matrix[]> children = crossover(parent1, parent2, netDriver);
                Matrix[] mutant1 = mutate(children.get(0), netDriver);
                Matrix[] mutant2 = mutate(children.get(1), netDriver);
                Population.add(mutant1);
                Population.add(mutant2);
            }
            //System.out.println("DONE");
        }

        
        for (int i = 0; i < Population.size(); i++ ) {
            //printInfo();
            ArrayList<Matrix[]> select = randomSelect(2);     
            Matrix[] parent1 = select.get(0);
            Matrix[] parent2 = select.get(1);
            ArrayList<Matrix[]> children = crossover(parent1, parent2, driver);
            Matrix[] mutant1 = mutate(children.get(0), driver);
            Matrix[] mutant2 = mutate(children.get(1), driver);
            replacement(parent1, mutant1, mutant2, in, net);
            replacement(parent2, mutant1, mutant2, in, net);
        }
        //selectBest(exampleIn, index);
    }
    
    private Matrix[] mutate(Matrix[] in, NeuralNetDriver netDriver) {
        Matrix[] mutant = in.clone();
        Random rand = new Random();    
        for (int i = 0; i < in.length; i++) {
            double next = rand.nextDouble(); //whether we mutate based on mute rate
            float mutate = (float) rand.nextGaussian(); //how much we mutate, need to add one since nextGaussian has mean 0, sd 1
            //System.out.println(mutate);
            if (next < net.getMutationRate()) {
                mutant[i] = in[i].madd(in[i].smul(mutate));
            }
        } 
        return mutant;
    }
    
    /*
    Single point crossover
    */
    private ArrayList<Matrix[]> crossover(Matrix[] parent1, Matrix[] parent2, NeuralNetDriver netDriver) { 
        ArrayList<Matrix[]> children = new ArrayList<>();
        Matrix[] child = parent1.clone();
        Matrix[] child2 = parent2.clone();
        double rate = net.getCrossoverRate();
        Random rand = new Random();
        
        for (int i = 0; i < parent1.length; i++ ) {
            double next = rand.nextDouble();
            child2[i] = parent1[i];
            if (next < rate) {
                for (int j = i; j < parent1.length - i; j++ ) {
                    child[j] = parent2[j];  
                }
                break;
            }
        }
        
        children.add(child);
        children.add(child2);
        return children;
    }   
}
