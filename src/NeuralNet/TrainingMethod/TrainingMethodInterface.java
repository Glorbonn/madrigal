package NeuralNet.TrainingMethod;
import DriverPack.NeuralNetDriver;
import NeuralNet.FeedforwardNetwork;
import Math.Matrix;
import java.util.ArrayList;
import javafx.util.Pair;
public interface TrainingMethodInterface {
    //MatrixNeuralNet Net = Driver.getNeuralNet();
    //Matrix errorMatrix = Net.errorMatrix;
    //ArrayList<ArrayList<Matrix>> test = Driver.getNetDriver().getValidation();
    //NeuralNetDriver netDriver = Driver.getNetDriver();
    //ArrayList<ArrayList<ArrayList<Pair<Matrix, Matrix>>>> train = netDriver.getTrain();
    //abstract void applyMethod(Matrix Example, Matrix output, int index); 
    abstract Matrix applyMethod(ArrayList<Pair<Matrix, Matrix>> in, FeedforwardNetwork Net, NeuralNetDriver N);
    //abstract void selectBest();
}
