package NeuralNet.TrainingMethod;
import DriverPack.Controller;
import DriverPack.MiniLib;
import Math.Matrix;
import NeuralNet.FeedforwardNetwork;
import DriverPack.NeuralNetDriver;
import java.util.ArrayList;
import javafx.util.Pair;
public class Backpropagation implements TrainingMethodInterface {   
    int counter = 0;
    float targOutArray[][];
    private Matrix errorMatrix;
    Matrix targetOutput;
    int batchCounter = 0;
    //public Matrix errorMatrix;
    //int trainSize = netDriver.train.size();
    boolean batch;
    public Backpropagation() {
        targOutArray = new float [1][Controller.in_outputSize];
        targetOutput = new Matrix(targOutArray);
        //targOutArray = new double [1][1];
    }
    @Override
    /*
    Does backprop and returns error matrix
    */
    public Matrix applyMethod(ArrayList<Pair<Matrix, Matrix>> in, FeedforwardNetwork net, NeuralNetDriver netDriver) {
        ArrayList<Matrix> helper = MiniLib.getKeys(in);
        Matrix M = new Matrix(helper);
        
        Matrix[] gradient = net.gradient;
        Matrix[] activationApplied = net.functionApplied;
        Matrix[] weights = net.weights;    


        targetOutput = new Matrix(MiniLib.getValues(in));

        
        errorMatrix = FeedforwardNetwork.errorMatrix.transpose();
        //System.out.println("Error in backprop function is: ");
        //errorMatrix.printMatrix();
        gradient[gradient.length - 1] = errorMatrix;//.transpose();
        for (int i = gradient.length-2; i > -1; i--) {
            /*System.out.println("element i " + i);
            System.out.println("====WEIGHT MATRIX====");
            weights[i+1].printMatrix();
            Matrix.checkNaN(weights[i+1]);


            System.out.println("====GRADI MATRIX====");
            gradient[i+1].printMatrix();
            Matrix.checkNaN(gradient[i+1]);

            System.out.println("====ACTIV MATRIX====");
            activationApplied[i].printMatrix();            
            Matrix.checkNaN(activationApplied[i]);*/

            
            
            gradient[i] = activationApplied[i].hadProd((weights[i+1].mmul(gradient[i+1])));
            //System.out.println(i);

            //System.out.println("Gradient magnitude is " + gradient[i].calculateMagnitude());
        }
        batchCounter++;
        updateWeights(net, M, netDriver);
        //updateBiases(net);
        return errorMatrix;
    }
    public void updateWeights(FeedforwardNetwork neuralNet, Matrix examples, NeuralNetDriver ND) {
        Matrix[] gradient = neuralNet.gradient;
        Matrix[] weights = neuralNet.weights;
        Matrix[] layerOutput = neuralNet.layerOutputWithActivationFunctionApplied;
        Matrix[] lastWeights = neuralNet.lastWeights;
        float eta = neuralNet.getEta(); // Math.pow(Net.exponent, Net.eta/*/Net.epochLimit*/);
        //System.out.println(eta);
        //eta = -eta;
        float momentum = neuralNet.momentumParameter;
        float weightMagnitude = 0;
        /*if (eta > 0) {
            System.out.println("ETA IS POSITIVE.");
        } else {
            System.out.println("ETA IS NEGATIVE.");
        }*/

        Matrix deltaW0 = gradient[0].mmul(examples).transpose();//(D[0].mmul((Matrix) example.getKey()).transpose()).madd(lastW[0]).smul(-1.0*eta);
        lastWeights[0] = deltaW0;
        weights[0] = weights[0].madd((deltaW0.smul(-eta)));
        //System.out.println("Weight Mangitude 0: " + deltaW0.calculateMagnitude());
        for (int i = 1; i < neuralNet.weights.length; i++) {
            //Matrix deltaW = ;
            //deltaW = D[i].mmul(neuralNet.Z[i-1]).transpose().madd(lastW[i]).smul(-1.0*eta);//(deltaWeight(neuralNet.Z[i - 1], D[i])).madd(lastW[i].smul(eta));
            //deltaW = gradient[i].mmul(layerOutput[i-1]).transpose();
            //System.out.println("Weight Magnitude " + i + ": " + deltaW.calculateMagnitude());
            weights[i] = weights[i].madd((gradient[i].mmul(layerOutput[i-1]).transpose()).smul(-eta));
            weights[i] = (weights[i].smul(1.0f -momentum)).madd((lastWeights[i].smul(momentum)));
            //System.out.println("Weight magnitude is " + weights[i].calculateMagnitude());
            //deltaW = deltaW.smul(1.0f - momentum).madd(lastWeights[i].smul(momentum));
            //System.out.println("Weight Magnitude mod " + i + ": " + deltaW.calculateMagnitude());
            /*lastWeights[i] = deltaW;
            weights[i] = weights[i].madd((deltaW).smul(momentum));
            weightMagnitude += weights[i].calculateMagnitude();*/
            //W[i].printMagnitude(i);
            //weights[i].printMatrix();
            lastWeights[i] = weights[i].copy();

        }
        
        //weights[0] = weights[0].madd(lastWeights[i])
        
        //System.out.println("Weight magnitude is " + weightMagnitude);
    }    
    
    public void updateBiases(FeedforwardNetwork neuralNet) {
        Matrix deltaBiasUpdate;
        float eta = neuralNet.getEta();
        float momentum = neuralNet.momentumParameter;
        //System.out.println(neuralNet.bias.length);
        for (int i = 1; i < neuralNet.bias.length; i++) {
            //System.out.println(i);
            deltaBiasUpdate = neuralNet.layerOutputWithActivationFunctionApplied[i-1].smul(-1.0f*eta).transpose();
                    
                  
            deltaBiasUpdate = deltaBiasUpdate.smul(1.0f - momentum).addBiasMatrices(neuralNet.lastBiasUpdates[i].smul(momentum));
            neuralNet.lastBiasUpdates[i] = deltaBiasUpdate;
            neuralNet.bias[i] = deltaBiasUpdate.addBiasMatrices(neuralNet.bias[i]);
        }
    }

    
    
    /*public void updateBiases(MatrixNeuralNet neuralNet) {
        Matrix deltaBiasUpdate;
        for (int i = 0; i < neuralNet.biasMatrices.length; i++) {
            deltaBiasUpdate = MatrixOperations.scalarMultiply(-1, MatrixOperations.scalarMultiply(neuralNet.eta,
                    MatrixOperations.transpose(neuralNet.deltaZMatrices[i])));
            deltaBiasUpdate = MatrixOperations.addBiasMatrices(deltaBiasUpdate, MatrixOperations.
                    scalarMultiply(neuralNet.momentumParameter, neuralNet.lastBiasUpdates[i]));
            neuralNet.lastBiasUpdates[i] = deltaBiasUpdate;
            neuralNet.biasMatrices[i] = MatrixOperations.addBiasMatrices(deltaBiasUpdate, neuralNet.biasMatrices[i]);
        }
    }*/


    
    /*public void printInfo() {
        netDriver.getError(train);
        netDriver.getError(test);
        if (counter % Driver.outputStep == 0 ) {
            System.out.format("\t%d\t Training Error: %1.10e\t Testing Error: %1.10e \n", counter, netDriver.getError(train), netDriver.getError(test));
            //System.out.printf("\t d \n", counter);
        }
        counter+=1;
    }*/

    /*
    finds delta for weights
     */
    /*public Matrix deltaWeight(Matrix zMatrix, Matrix M) {
        double eta = Driver.eta;
        Matrix deltaWeightMatrix = M.mmul(zMatrix).transpose();
        return deltaWeightMatrix;
    }*/

}
